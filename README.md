Project by Nassim Lougrada & Paul Petit-Jove

Achievement list:

- Setting up the project
- Setting up spritesheet method
- Create basic player ship
- Create projectiles
- Write colliders code
- Create hostile objects
- Adding ennemy moving patterns
- Create projectiles pattern
- Create firing modules using trajectories
- Adding ennemy firing patterns
- Adding background
- Adding background scroller
- Adding player ship reactor animation
- Add Point to GameObject determining projectiles origin when fired

In progress:

- Setting up interface
- Setting up sound player

Next:

- Adding environment animation