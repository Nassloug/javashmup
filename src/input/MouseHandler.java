package input;

import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

import org.lwjgl.glfw.GLFWMouseButtonCallback;

public class MouseHandler extends GLFWMouseButtonCallback{

	public static boolean[] buttons = new boolean[65536];
	
	@Override
	public void invoke(long window, int button, int action, int mods) {
		// TODO Auto-generated method stub
		buttons[button] = action != GLFW_RELEASE;
	}
	
	public static boolean isButtonDown(int button) {
		return buttons[button];
	}

}
