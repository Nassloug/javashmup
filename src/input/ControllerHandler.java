package input;

import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Event;
import net.java.games.input.EventQueue;

public class ControllerHandler{

  public final static float CONTROLLER_TRESHOLD=0.1f;
  public static float dx,dy;
  public static boolean a,b;
  
  public static void updateControllerState() {
    Controller[] ca = ControllerEnvironment.getDefaultEnvironment().getControllers();
    for(int i =0;i<ca.length;i++){
      if(ca[i].getType()==Controller.Type.GAMEPAD){
        ca[i].poll();
        EventQueue queue = ca[i].getEventQueue();
        Event event = new Event();
        while(queue.getNextEvent(event)){
          /** Xbox controller IDs
           * (x,y)=LJoystickState
           * z<0=LTrigger
           * z>0=RTrigger
           * 0=A,1=B,2=X,3=Y,
           * 4=LButton,5=RButton,
           * 6=Select,7=Start
           * 8=LJoystickClick,9=RJoystickClick
           * pov=DPad
           **/
          switch(""+event.getComponent().getIdentifier()){
            case "0":a=event.getValue()==1.0f;break;
            case "1":b=event.getValue()==1.0f;break;
            case "x":dx=event.getValue();break;
            case "y":dy=event.getValue();break;
            default:break;
          }
          /**
            String state=ca[i].getName();
            state+=" at ";
            state+=event.getNanos()+", ";
            Component comp = event.getComponent();
            state+=comp.getIdentifier()+" changed to ";
            float value = event.getValue(); 
            state+=value;
            System.out.println(state.toString());
           **/
        }
      }
    }
  }

}
