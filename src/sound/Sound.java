package sound;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Sound {
  
  private AudioInputStream audioInputStream;
  
  private AudioFormat audioFormat;
  
  private Clip clip;
  
  private final int NOT_SPECIFIED=AudioSystem.NOT_SPECIFIED;
  private int sampleSize=NOT_SPECIFIED;
  private int channelsNum;
  
  private long pausedAt=-1;
  private long framesCount=NOT_SPECIFIED;
  
  private byte[] wavData;

  private String soundPath;
  
  public Sound(String soundPath)throws IOException,UnsupportedAudioFileException, LineUnavailableException, InterruptedException {
    this.soundPath=soundPath;
    File soundFile=new File("sounds/"+soundPath);
    audioInputStream=AudioSystem.getAudioInputStream(soundFile);
    
    audioFormat=audioInputStream.getFormat();
    framesCount=audioInputStream.getFrameLength();
    sampleSize=audioFormat.getSampleSizeInBits()/8;
    channelsNum=audioFormat.getChannels();
    long dataLength=framesCount*audioFormat.getSampleSizeInBits()*audioFormat.getChannels()/8;
    wavData=new byte[(int)dataLength];
    audioInputStream.read(wavData);
    audioInputStream=AudioSystem.getAudioInputStream(soundFile);
    clip=AudioSystem.getClip();
    clip.open(audioInputStream);
  }
  
  public int getAmplitude(int frameNumber){
    if(frameNumber<0||frameNumber*sampleSize*channelsNum+sampleSize>=wavData.length){
      return -1;//invalid sampleNumber
    }
    byte[] sampleBytes=new byte[4];
    for(int i=0;i<sampleSize;i++){
      sampleBytes[i]=wavData[frameNumber*sampleSize*channelsNum+i];
    }
    int sample=ByteBuffer.wrap(sampleBytes).order(ByteOrder.LITTLE_ENDIAN).getInt();
    return sample;
  }
  
  public int getAmplitude(){
    return getAmplitude(clip.getFramePosition()%clip.getFrameLength());
  }
  
  public boolean play(){
    if(pausedAt!=-1){
      clip.setMicrosecondPosition(pausedAt);
      clip.start();
      pausedAt=-1;
      return true;
    }else{
      if(!clip.isRunning()){
        if(clip.getMicrosecondLength()<=clip.getMicrosecondPosition())clip.setMicrosecondPosition(0);
        clip.start();
        return true;
      }else{
        return false;
      }
    }
  }
  
  public boolean loop(){
    clip.loop(Clip.LOOP_CONTINUOUSLY);
    return true;
  }
  
  public void pause(){
    if(clip.isRunning()){
      clip.stop();
      pausedAt=clip.getMicrosecondPosition();
    }
  }
  
  public void close(){
    clip.drain();
    clip.close();
    try {
      audioInputStream.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  public void setVolume(float amount){
    FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
    float mod=gainControl.getValue()+amount;
    if(mod>gainControl.getMinimum()&&mod<gainControl.getMaximum())gainControl.setValue(mod);//amount correspond aux décibels
  }
  
  public float getVolume(){
    return ((FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN)).getValue();
  }
  
  public String getSoundPath() {
    return soundPath;
  }

  public void setSoundPath(String soundPath) {
    this.soundPath = soundPath;
  }
  
  public String toString(){
    return "Sound from \'"+soundPath+"\' specs : "+
        "\naudioFormat "+audioFormat+
        "\nframesCount "+framesCount+
        "\nsampleSize "+sampleSize+
        "\nchannelsNum "+channelsNum;
  }
}
