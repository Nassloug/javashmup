package sound;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

//format supportés : aiff, au et wav
public class SoundPlayer {
  private Map<String,List<Sound>> sounds=new HashMap<String,List<Sound>>();
  
  public void addSound(String soundPath,String soundName){
    try{
      List<Sound> al=new ArrayList<Sound>();
      al.add(new Sound(soundPath));
      sounds.put(soundName,al);
    }catch(Exception e){e.printStackTrace();}
  }
  
  public void playSound(String soundName){
    boolean hasPlayed=false;
    List<Sound> ls=sounds.get(soundName);
    if(ls==null)ls=sounds.get("missing_sound");
    for(Sound s:ls){
      hasPlayed=s.play();
      if(hasPlayed){
        break;
      }
    }
    if(!hasPlayed){
      Sound s;
      try {
        s = new Sound(ls.get(0).getSoundPath());
        s.setVolume(ls.get(0).getVolume());
        ls.add(s);
        s.play();
      } catch (IOException | UnsupportedAudioFileException | LineUnavailableException | InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
  
  public void playSoundForever(String soundName){
    boolean hasPlayed=false;
    List<Sound> ls=sounds.get(soundName);
    if(ls==null)ls=sounds.get("missing_sound");
    for(Sound s:ls){
      hasPlayed=s.loop();
      if(hasPlayed){
        break;
      }
    }
    if(!hasPlayed){
      Sound s;
      try {
        s = new Sound(ls.get(0).getSoundPath());
        s.setVolume(ls.get(0).getVolume());
        ls.add(s);
        s.loop();
      } catch (IOException | UnsupportedAudioFileException | LineUnavailableException | InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
  
  public void pauseSound(String soundName){
    List<Sound> ls=sounds.get(soundName);
    for(Sound s:ls){
      s.pause();
    }
  }
  
  public void setVolumeSound(String soundName,float amount){
    List<Sound> ls=sounds.get(soundName);
    for(Sound s:ls){
      s.setVolume(amount);
    }
  }
  
  public void removeSound(String soundName){
    List<Sound> ls=sounds.get(soundName);
    for(Sound s:ls){
      s.close();
    }
    sounds.remove(soundName);
  }
  
  public List<Sound> getSound(String soundName){
    return sounds.get(soundName);
  }
}
