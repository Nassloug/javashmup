package render;

import res.SpriteSheet;

public class Animation {
	
	private int animState,animTimer,repeatStep,frames;
	private int animWidth,animHeight;
	private boolean stopAnim, ended;
	private String[] sprites;
	
	public Animation(String[] sprites){
		animState = 0;
		animTimer = 0;
		repeatStep = 0;
		stopAnim = false;
		ended = false;
		this.sprites = sprites;
		frames = sprites.length;
		animWidth = SpriteSheet.sprites.get(sprites[0]).getWidth();
		animHeight = SpriteSheet.sprites.get(sprites[0]).getHeight();
	}

	public int getAnimState() {
		return animState;
	}

	public void setAnimState(int animState) {
		this.animState = animState;
	}

	public int getAnimTimer() {
		return animTimer;
	}

	public void setAnimTimer(int animTimer) {
		this.animTimer = animTimer;
	}

	public void nextAnimStep(){
		this.animState++;
	}

	public void incrementAnimTimer(){
		this.animTimer++;
	}
	
	public void startAnim(){
		stopAnim = false;
	}

	public void stopAnim(){
		stopAnim = true;
	}

	public int getAnimWidth() {
		return animWidth;
	}

	public void setAnimWidth(int animWidth) {
		this.animWidth = animWidth;
	}

	public int getAnimHeight() {
		return animHeight;
	}

	public void setAnimHeight(int animHeight) {
		this.animHeight = animHeight;
	}

	public String getSprite(){
		return sprites[animState];
	}
	
	 public void setSprite(String[] sprites){
	    this.sprites=sprites;
	  }
	
	public boolean isEnded(){
		return ended;
	}
	
	public void end(){
		stopAnim = true;
		ended = true;
	}

	public void animate(int repeat, boolean loop, boolean reset, double animSpeed){
		if(!stopAnim){
			if(animTimer >= GameRenderer.ANIM_RATE*2*animSpeed){
				if(animState<frames-1){
					animState++;
					animTimer = 0;
				}else if(repeat>0&&repeatStep<repeat){
					animState = 0;
					animTimer = 0;
					repeatStep++;
				}else if(loop){
					animState = 0;
					animTimer = 0;
				}else if(reset){
					animState = 0;
					repeatStep = 0;
					stopAnim = true;
				}else{
					repeatStep = 0;
					stopAnim = true;
					ended = true;
				}
			}
			animTimer++;
		}
	}
	
	public Animation clone(){
		return new Animation(sprites);
	}
	
}
