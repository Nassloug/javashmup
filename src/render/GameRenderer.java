package render;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetTime;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwGetWindowSize;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.ARBTextureRectangle.GL_TEXTURE_RECTANGLE_ARB;
import static org.lwjgl.opengl.GL11.GL_BACK;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glCullFace;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.nio.IntBuffer;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryStack;

import core.GameEngine;
import core.levels.Level;
import core.objects.TextPlate;
import input.KeyboardHandler;
import input.MouseHandler;
import render.layer.Layer;
import res.SpriteSheet;

public class GameRenderer{

	public static final int REFRESH_RATE = 60;
	public static final int ANIM_RATE = REFRESH_RATE/10;
	public static final int MAX_TEXTURES = 16;
	public static int[] textureIDs;
	
	public static Level level;
	public static long window;

	private static double lastTime = glfwGetTime();
	private static double time = 0;
	private static double deltaTime = 1.0/REFRESH_RATE;
	
	private static int x,y,x2,y2;
	private static float xc,yc,x2c,y2c;

	private static GLFWKeyCallback keyCallback;
	private static GLFWMouseButtonCallback mouseButtonCallback;

	public static boolean running = true, debug = false;

	public static void setup(){
		setUpDisplay();
		setUpStates();
		setUpMatrices();
	}	

	private static void setUpStates(){
		glEnable(GL_TEXTURE_RECTANGLE_ARB);
		glEnable(GL_CULL_FACE);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glCullFace(GL_BACK);
	}

	private static void setUpMatrices(){
		glMatrixMode(GL_PROJECTION);
		glOrtho(0.0f, GameEngine.winW, GameEngine.winH, 0.0f, -1f, 1f);
		glMatrixMode(GL_MODELVIEW);
	}

	private static void setUpDisplay() {
		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure GLFW
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

		// Create the window
		window = glfwCreateWindow(GameEngine.winW, GameEngine.winH, "Shmup!", NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		// Setup a key callback. It will be called every time a key is pressed, repeated or released.
		glfwSetKeyCallback(window, keyCallback = new KeyboardHandler());

		glfwSetMouseButtonCallback(window, mouseButtonCallback = new MouseHandler());
		
		// Get the thread stack and push a new frame
		try ( MemoryStack stack = stackPush() ) {
			IntBuffer pWidth = stack.mallocInt(1); // int*
			IntBuffer pHeight = stack.mallocInt(1); // int*

			// Get the window size passed to glfwCreateWindow
			glfwGetWindowSize(window, pWidth, pHeight);

			// Get the resolution of the primary monitor
			GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

			// Center the window
			glfwSetWindowPos(
					window,
					(vidmode.width() - pWidth.get(0)) / 2,
					(vidmode.height() - pHeight.get(0)) / 2
					);
		} // the stack frame is popped automatically

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		GL.createCapabilities();

		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(window);

		// Generate texture IDs
		textureIDs = new int[MAX_TEXTURES];
		for(int i=0;i<textureIDs.length;i++){
			textureIDs[i] = glGenTextures(); 
		}
	}

	public static void loadLevel(Level newLevel){

		level = newLevel;
		level.loadLevel();

		int i=0;
		
		for(Layer layer : level.layers){
			layer.texture = TextureLoader.loadTexture(TextureLoader.loadImage(layer.texturePath),i);
			i++;
		}
		TextureLoader.loadTexture(TextureLoader.loadImage("img/sprites/Font2.png"),i);
	}
	
	private static void enterGameLoop() {
		while (!glfwWindowShouldClose(window)) {
			time = glfwGetTime();
			if(time-lastTime>=deltaTime){
				update();
				render();
				lastTime = time;
			}
		}
	}

	public static void run() {
		
		enterGameLoop();

		// Free the window callbacks and destroy the window
		glfwFreeCallbacks(window);
		glfwDestroyWindow(window);

		// Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();
	}
	
	public static void update(){
	  try{
	    glfwPollEvents();
	  }catch(ArrayIndexOutOfBoundsException AIOOBE){
	    //System.out.println("unhandled key");
	  }
		GameEngine.update();
	}

	public static void render(){
		glfwSwapBuffers(window);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		level.drawLayers();

		// Text Overlay
		glBindTexture(GL_TEXTURE_RECTANGLE_ARB, level.layers.size()+1);
		for(TextPlate tp : GameEngine.textPlates){
			drawText(tp.getText(),tp.getX(),tp.getY(),tp.getSpacing());
		}
		//fps
		if(debug)drawText(""+(int)(1f/(time-lastTime)),16,16,0.25f);
	}

	public static void drawText(String text,int tx,int ty,float letterSpacing){
    for(int idx=0;idx<text.length();idx++){
      char c=text.charAt(idx);
      int spriteWidth=SpriteSheet.sprites.get(""+c).getWidth(),
          spriteHeight=SpriteSheet.sprites.get(""+c).getHeight();
      x = SpriteSheet.sprites.get(""+c).getX();
      y = SpriteSheet.sprites.get(""+c).getY();
      x2 = x + spriteWidth;
      y2 = y + spriteHeight;

      xc = tx-text.length()*(spriteWidth*letterSpacing)/2+idx*(spriteWidth*letterSpacing);
      yc = ty;
      x2c = xc + spriteWidth;
      y2c = yc + spriteHeight;

      glBegin(org.lwjgl.opengl.GL11.GL_QUADS);

      glTexCoord2f(x, y);
      glVertex2f(xc, yc);
      glTexCoord2f(x, y2);
      glVertex2f(xc, y2c);
      glTexCoord2f(x2, y2);
      glVertex2f(x2c, y2c);
      glTexCoord2f(x2, y);
      glVertex2f(x2c, yc);

      glEnd();
    }
	}
}