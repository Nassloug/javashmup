package render.gui;

import core.events.Event;

public class Button extends GUIElement{

	public Button(int x, int y, int width, int height, String[] sprites, Event event) {
		super(x, y, width, height, true, sprites);
		this.event = event;
	}

	@Override
	public void performAction() {
		event.happen();
	}

}
