package render.gui;

import static org.lwjgl.glfw.GLFW.glfwGetCursorPos;

import java.nio.DoubleBuffer;

import org.lwjgl.BufferUtils;

import core.events.Event;
import render.GameRenderer;
import res.Sprite;
import res.SpriteSheet;

public abstract class GUIElement {

	protected int x,y,width,height;
	protected boolean clickable,over;
	protected Event event;
	protected Sprite[] sprites;
	protected Sprite currentSprite;

	public GUIElement(int x, int y, int width, int height, boolean clickable, String[] sprites){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.clickable = clickable;
		this.sprites = new Sprite[sprites.length];
		for(int i=0;i<sprites.length;i++){
			this.sprites[i] = SpriteSheet.sprites.get(sprites[i]);
		}
		currentSprite = this.sprites[0];
		over = false;
	}

	public abstract void performAction();

	public boolean isCursorOver(){
		DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
		DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
		glfwGetCursorPos(GameRenderer.window, xBuffer, yBuffer);
		int xCursor = (int)xBuffer.get(0);
		int yCursor = (int)yBuffer.get(0);
		if(doesCollide(xCursor,yCursor,0,0)){
			over = true;
			if(clickable){
				currentSprite = sprites[1];
			}
		}else{
			over = false;
			currentSprite = sprites[0];
		}
		return over;
	}

	public boolean doesCollide(int otherX, int otherY, int otherWidth, int otherHeight){
		if(otherX>x+width){
			return false;
		}else if(otherX+otherWidth<x){
			return false;
		}else if(otherY>y+height){
			return false;
		}else if(otherY+otherHeight<y){
			return false;
		}else{
			return true;
		}
	}
}
