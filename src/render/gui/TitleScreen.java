package render.gui;

import static org.lwjgl.opengl.ARBTextureRectangle.GL_TEXTURE_RECTANGLE_ARB;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;

import java.util.ArrayList;

import org.lwjgl.glfw.GLFW;

import core.GameEngine;
import core.events.LevelSwitch;
import core.levels.Mission00;
import core.levels.Mission01;
import input.MouseHandler;
import render.layer.BackgroundLayer;
import res.SpriteSheet;

public class TitleScreen extends GraphicalUserInterface{
	
	public TitleScreen() {
		super("img/sprites/title-screen.png");
	}

	@Override
	public void drawLayer() {

		glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture);
		
		for(GUIElement element : elements){
		
			x = element.currentSprite.getX();
			y = element.currentSprite.getY();
			x2 = element.currentSprite.getX() + element.currentSprite.getWidth();
			y2 = element.currentSprite.getY() + element.currentSprite.getHeight();
	
			glBegin(GL_QUADS);
	
			xc = element.x;
			x2c = element.x + element.width;
			yc = element.y;
			y2c = element.y + element.height;
	
			glTexCoord2f(x, y);
			glVertex2f(xc, yc);
			glTexCoord2f(x, y2);
			glVertex2f(xc, y2c);
			glTexCoord2f(x2, y2);
			glVertex2f(x2c, y2c);
			glTexCoord2f(x2, y);
			glVertex2f(x2c, yc);
	
			glEnd();
		}
		
	}

	@Override
	public void update() {
		for(GUIElement element : elements){
			if(element.clickable && element.isCursorOver() && MouseHandler.isButtonDown(GLFW.GLFW_MOUSE_BUTTON_LEFT)){
				element.performAction();
			}
		}
		
	}

	@Override
	public void loadInterface() {
		
		SpriteSheet.loadSprite("title-screen-0", 0, 0, 640, 1280);
		SpriteSheet.loadSprite("title-screen-1", 640, 0, 640, 160);
		SpriteSheet.loadSprite("title-screen-2", 1280, 0, 640, 160);
		
		level.layers.add(new BackgroundLayer(640, 1280, 0, 0, 0, 0, 0, new String[]{"title-screen-0"}, null, null, "img/sprites/title-screen.png"));
		overlay = false;
		elements = new ArrayList<GUIElement>();
		int width = GameEngine.winW/2;
		int height = GameEngine.winH/10;
		int x = GameEngine.winW/2 - width/2;
		int y = GameEngine.winH/2 - height/2;
		LevelSwitch event = new LevelSwitch(true,level,new Mission00());
		elements.add(new Button(x,y,width,height,new String[]{"title-screen-1","title-screen-2"},event));
		elements.add(new Button(x,y+2*height,width,height,new String[]{"title-screen-1","title-screen-2"},new LevelSwitch(true,level,new Mission01())));
	}
	
}
