package render.gui;

import java.util.List;

import core.levels.Level;
import render.layer.Layer;

public abstract class GraphicalUserInterface extends Layer{

	protected boolean overlay;
	protected List<GUIElement> elements;
	protected Level level;
	
	public GraphicalUserInterface(String layerSpriteSheet){
		super(layerSpriteSheet);
	}
	
	public abstract void loadInterface();
	
	public abstract void drawLayer();
	
	public void setLevel(Level level){
		this.level = level;
	}
	
}
