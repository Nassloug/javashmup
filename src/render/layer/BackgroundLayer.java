package render.layer;

import static org.lwjgl.opengl.ARBTextureRectangle.GL_TEXTURE_RECTANGLE_ARB;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;

import core.GameEngine;
import render.GameRenderer;
import res.Sprite;
import res.SpriteSheet;

public class BackgroundLayer extends Layer{

	private int maxWidth,maxHeight,maxBackWidth,maxBackHeight,maxOverWidth,maxOverHeight,
				scrollSpeed,animSpeed,overlaySpeed,
				animScrollSpeed,overlayScrollSpeed,
				animState,animTimer,overlayTimer,repeatAnimStep;
	private int xAnim,yAnim,widthAnim,heightAnim;
	private Sprite[] backSprites;
	private Sprite[] animSprites;
	private Sprite[] overlaySprites;
	private int[] xBack,yBack,widthBack,heightBack,xOver,yOver,widthOver,heightOver;
	private int backFrame,overlayFrame;
	private double overlayRatio,backRatio;
	private boolean stopAnim,stopOverlay;

	public BackgroundLayer(int width, int height, int scrollSpeed, int animSpeed, int animScrollSpeed, int overlaySpeed, int overlayScrollSpeed, String[] backSprites, String[] animSprites, String[] overlaySprites, String texturePath){
		
		super(texturePath);
		
		maxOverWidth=maxOverHeight=maxBackHeight=maxBackWidth=0;
		overlayRatio=backRatio=0;
		
		// Background sprites initialization
		
		if(backSprites!=null){
			if(backSprites.length<=1){
				this.backSprites = new Sprite[2];
				this.backSprites[0] = SpriteSheet.sprites.get(backSprites[0]).newCopy();
				this.backSprites[1] = SpriteSheet.sprites.get(backSprites[0]).newCopy();
			}else{
				this.backSprites = new Sprite[backSprites.length];
				for(int i=0;i<backSprites.length;i++){
					this.backSprites[i] = SpriteSheet.sprites.get(backSprites[i]).newCopy();
				}
			}
			xBack = new int[this.backSprites.length];
			for(int i=0;i<xBack.length;i++){
				xBack[i]=0;
			}
			yBack = new int[this.backSprites.length];
			for(int i=0;i<yBack.length;i++){
				yBack[i]=0;
				if(i>0){
					this.backSprites[i].setY(height);
				}
			}
			this.widthBack = new int[this.backSprites.length];
			for(int i=0;i<widthBack.length;i++){
				widthBack[i]=width;
			}
			this.heightBack = new int[this.backSprites.length];
			for(int i=0;i<heightBack.length;i++){
				if(i>0){
					this.backSprites[i].setHeight(0);
					heightBack[i]=0;
				}else{
					heightBack[i]=height;
				}
			}
			maxBackHeight = this.backSprites[0].getHeight();
			maxBackWidth = this.backSprites[0].getWidth();
			backRatio = (double)(maxBackHeight)/height;
		}
		
		// Overlay sprites initialization
		
		if(overlaySprites!=null){
			if(overlaySprites.length<=1){
				this.overlaySprites = new Sprite[2];
				this.overlaySprites[0] = SpriteSheet.sprites.get(overlaySprites[0]).newCopy();
				this.overlaySprites[1] = SpriteSheet.sprites.get(overlaySprites[0]).newCopy();
			}else{
				this.overlaySprites = new Sprite[overlaySprites.length];
				for(int i=0;i<overlaySprites.length;i++){
					this.overlaySprites[i] = SpriteSheet.sprites.get(overlaySprites[i]).newCopy();
				}
			}
			xOver = new int[this.overlaySprites.length];
			for(int i=0;i<xOver.length;i++){
				xOver[i]=0;
			}
			yOver = new int[this.overlaySprites.length];
			for(int i=0;i<yOver.length;i++){
				yOver[i]=0;
				if(i>0){
					this.overlaySprites[i].setY(height);
				}
			}
			this.widthOver = new int[this.overlaySprites.length];
			for(int i=0;i<widthOver.length;i++){
				widthOver[i]=width;
			}
			this.heightOver = new int[this.overlaySprites.length];
			for(int i=0;i<heightOver.length;i++){
				if(i>0){
					this.overlaySprites[i].setHeight(0);
					heightOver[i]=0;
				}else{
					heightOver[i]=height;
				}
			}
			maxOverHeight = this.overlaySprites[0].getHeight();
			maxOverWidth = this.overlaySprites[0].getWidth();
			overlayRatio = (double)(maxOverHeight)/height;
		}
		animState=animTimer=repeatAnimStep=overlayTimer=0;
		if(animSprites!=null){
			this.animSprites = new Sprite[animSprites.length];
			for(int i=0;i<animSprites.length;i++){
				this.animSprites[i] = SpriteSheet.sprites.get(animSprites[i]).newCopy();
			}
			heightAnim = this.animSprites[0].getHeight();
			widthAnim = this.animSprites[0].getWidth();
			yAnim=-heightAnim;
		}
		this.scrollSpeed = scrollSpeed;
		this.animSpeed = animSpeed;
		this.overlaySpeed = overlaySpeed;
		this.animScrollSpeed = animScrollSpeed;
		this.overlayScrollSpeed = overlayScrollSpeed;
		maxWidth = width;
		maxHeight = height;
		backFrame=overlayFrame=0;
		stopAnim = false;
		stopOverlay = false;
	}

	public void update(){
		scrollBackground();
		scrollOverlay();
		if(animSpeed!=0){
			anim(animSprites.length,0,true,false);
		}
	}

	private void scrollBackground(){
		if(scrollSpeed!=0){
			if(backFrame<backSprites.length-1){
				yBack[backFrame]+=scrollSpeed;
				heightBack[backFrame]-=scrollSpeed;
				backSprites[backFrame].setHeight((int)(heightBack[backFrame]*backRatio));
				heightBack[backFrame+1]+=scrollSpeed;
				backSprites[backFrame+1].setY(backSprites[backFrame+1].getOriginalY()+(int)(maxHeight-yBack[backFrame]*backRatio));
				backSprites[backFrame+1].setHeight((int)(heightBack[backFrame+1]*backRatio));
				if(heightBack[backFrame]<=0){
					yBack[backFrame]=0;
					heightBack[backFrame]=0;
					heightBack[backFrame+1]=maxHeight;
					backSprites[backFrame+1].resetSprite();
					backSprites[backFrame].setHeight(0);
					backSprites[backFrame].setY(backSprites[backFrame].getOriginalY()+maxHeight);
					backFrame++;
				}
			}else{
				yBack[backFrame]+=scrollSpeed;
				heightBack[backFrame]-=scrollSpeed;
				backSprites[backFrame].setHeight((int)(heightBack[backFrame]*backRatio));
				heightBack[0]+=scrollSpeed;
				backSprites[0].setY(backSprites[0].getOriginalY()+(int)(maxHeight-yBack[backFrame]*backRatio));
				backSprites[0].setHeight((int)(heightBack[0]*backRatio));
				if(heightBack[backFrame]<=0){
					yBack[backFrame]=0;
					heightBack[backFrame]=0;
					heightBack[0]=maxHeight;
					backSprites[0].resetSprite();
					backSprites[backFrame].setHeight(0);
					backSprites[backFrame].setY(backSprites[backFrame].getOriginalY()+maxHeight);
					backFrame=0;
				}
			}
		}
		if(animScrollSpeed!=0){
			yAnim+=animScrollSpeed;
			if(yAnim>GameEngine.winH){
				yAnim=(int)(-heightAnim*Math.random()*10);
			}
		}
	}
	
	private void scrollOverlay(){
		if(overlayScrollSpeed!=0){
			if(overlayFrame<overlaySprites.length-1){
				yOver[overlayFrame]+=overlayScrollSpeed;
				heightOver[overlayFrame]-=overlayScrollSpeed;
				overlaySprites[overlayFrame].setHeight((int)(heightOver[overlayFrame]*overlayRatio));
				heightOver[overlayFrame+1]+=overlayScrollSpeed;
				overlaySprites[overlayFrame+1].setY(overlaySprites[overlayFrame+1].getOriginalY()+(int)((maxOverHeight-yOver[overlayFrame]*overlayRatio)));
				overlaySprites[overlayFrame+1].setHeight((int)(heightOver[overlayFrame+1]*overlayRatio));
				if(heightOver[overlayFrame]<=0){
					yOver[overlayFrame]=0;
					heightOver[overlayFrame]=0;
					heightOver[overlayFrame+1]=maxHeight;
					overlaySprites[overlayFrame+1].resetSprite();
					overlaySprites[overlayFrame].setHeight(0);
					overlaySprites[overlayFrame].setY(overlaySprites[overlayFrame].getOriginalY()+maxOverHeight);
					overlayFrame++;
				}
			}else{
				yOver[overlayFrame]+=overlayScrollSpeed;
				heightOver[overlayFrame]-=overlayScrollSpeed;
				overlaySprites[overlayFrame].setHeight((int)(heightOver[overlayFrame]*overlayRatio));
				heightOver[0]+=overlayScrollSpeed;
				overlaySprites[0].setY(overlaySprites[0].getOriginalY()+(int)(maxOverHeight-yOver[overlayFrame]*overlayRatio));
				overlaySprites[0].setHeight((int)(heightOver[0]*overlayRatio));
				if(heightOver[overlayFrame]<=0){
					yOver[overlayFrame]=0;
					heightOver[overlayFrame]=0;
					heightOver[0]=maxHeight;
					overlaySprites[0].resetSprite();
					overlaySprites[overlayFrame].setHeight(0);
					overlaySprites[overlayFrame].setY(overlaySprites[overlayFrame].getOriginalY()+maxOverHeight);
					overlayFrame=0;
				}
			}
		}
	}

	public void anim(int frames, int repeat, boolean loop, boolean reset){
		if(!stopAnim){
			if(animTimer >= GameRenderer.ANIM_RATE*animSpeed){
				if(animState<frames-1){
					animState++;
					animTimer=0;
				}else if(repeat>0&&repeatAnimStep<repeat){
					animState=0;
					animTimer=0;
					repeatAnimStep++;
				}else if(loop){
					animState=0;
					animTimer=0;
				}else if(reset){
					animState=0;
					repeatAnimStep = 0;
					stopAnim = true;
				}else{
					repeatAnimStep = 0;
					stopAnim = true;
				}
			}
			animTimer++;
		}
	}

	public void drawLayer(){

		glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture);

		if(backSprites!=null){

			int i=0;

			for(Sprite s : backSprites){

				// Sprite coordinates initialization
				x = s.getX();
				y = s.getY();
				x2 = s.getX() + s.getWidth();
				y2 = s.getY() + s.getHeight();

				glBegin(GL_QUADS);

				// Object coordinates initialization
				xc = xBack[i];
				yc = yBack[i];
				x2c = xBack[i]+widthBack[i];
				y2c = yBack[i]+heightBack[i];

				glTexCoord2f(x, y);
				glVertex2f(xc, yc);
				glTexCoord2f(x, y2);
				glVertex2f(xc, y2c);
				glTexCoord2f(x2, y2);
				glVertex2f(x2c, y2c);
				glTexCoord2f(x2, y);
				glVertex2f(x2c, yc);

				glEnd();

				i++;

			}

		}

		// Background animation

		if(animSprites!=null){

			x = getAnimSprite().getX();
			y = getAnimSprite().getY();
			x2 = getAnimSprite().getX() + getAnimSprite().getWidth();
			y2 = getAnimSprite().getY() + getAnimSprite().getHeight();

			glBegin(GL_QUADS);

			xc = getXAnim();
			yc = getYAnim();
			x2c = getXAnim()+getWidthAnim();
			y2c = getYAnim()+getHeightAnim();

			glTexCoord2f(x, y);
			glVertex2f(xc, yc);
			glTexCoord2f(x, y2);
			glVertex2f(xc, y2c);
			glTexCoord2f(x2, y2);
			glVertex2f(x2c, y2c);
			glTexCoord2f(x2, y);
			glVertex2f(x2c, yc);

			glEnd();

		}

		// Background overlay

		if(overlaySprites!=null){

			int i=0;

			for(Sprite s : overlaySprites){

				// Sprite coordinates initialization
				x = s.getX();
				y = s.getY();
				x2 = s.getX() + s.getWidth();
				y2 = s.getY() + s.getHeight();

				glBegin(GL_QUADS);

				// Object coordinates initialization
				xc = xOver[i];
				yc = yOver[i];
				x2c = xOver[i]+widthOver[i];
				y2c = yOver[i]+heightOver[i];

				glTexCoord2f(x, y);
				glVertex2f(xc, yc);
				glTexCoord2f(x, y2);
				glVertex2f(xc, y2c);
				glTexCoord2f(x2, y2);
				glVertex2f(x2c, y2c);
				glTexCoord2f(x2, y);
				glVertex2f(x2c, yc);

				glEnd();

				i++;

			}

		}
	}

	public void stopAnim(){
		stopAnim = true;
	}

	public void startAnim(){
		stopAnim = false;
	}

	public void stopOverlay(){
		stopOverlay = true;
	}

	public void startOverlay(){
		stopOverlay = false;
	}

	public int getXAnim(){
		return xAnim;
	}

	public int getYAnim(){
		return yAnim;
	}

	public int getHeightAnim(){
		return heightAnim;
	}

	public int getWidthAnim(){
		return widthAnim;
	}

	public int getXOver(int idx){
		return xOver[idx];
	}

	public int getYOver(int idx){
		return yOver[idx];
	}

	public int getHeightOver(int idx){
		return heightOver[idx];
	}

	public int getWidthOver(int idx){
		return widthOver[idx];
	}

	public Sprite[] getOverlaySprites(){
		return overlaySprites;
	}

	public int getXBack(int idx){
		return xBack[idx];
	}

	public int getYBack(int idx){
		return yBack[idx];
	}

	public int getHeightBack(int idx){
		return heightBack[idx];
	}

	public int getWidthBack(int idx){
		return widthBack[idx];
	}

	public Sprite[] getBackSprites(){
		return backSprites;
	}

	public Sprite getAnimSprite(){
		return animSprites[animState];
	}

}
