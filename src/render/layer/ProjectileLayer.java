package render.layer;

import static org.lwjgl.opengl.ARBTextureRectangle.GL_TEXTURE_RECTANGLE_ARB;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;

import java.util.ArrayList;

import core.GameEngine;
import core.objects.Explosion;
import core.objects.projectiles.Projectile;
import render.GameRenderer;
import res.SpriteSheet;

public class ProjectileLayer extends Layer{

	public ProjectileLayer(String texturePath) {
		super(texturePath);
	}

	@Override
	public void drawLayer() {
		glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture);

		for(Projectile p: GameEngine.player.projectiles){
			
			x = SpriteSheet.sprites.get(p.getAnim().getSprite()).getX();
			y = SpriteSheet.sprites.get(p.getAnim().getSprite()).getY();
			x2 = SpriteSheet.sprites.get(p.getAnim().getSprite()).getX() + SpriteSheet.sprites.get(p.getAnim().getSprite()).getWidth();
			y2 = SpriteSheet.sprites.get(p.getAnim().getSprite()).getY() + SpriteSheet.sprites.get(p.getAnim().getSprite()).getHeight();

			glBegin(GL_QUADS);

			xc = p.getX();
			yc = p.getY();
			x2c = p.getX()+p.getWidth();
			y2c = p.getY()+p.getHeight();

			glTexCoord2f(x, y);
			glVertex2f(xc, yc);
			glTexCoord2f(x, y2);
			glVertex2f(xc, y2c);
			glTexCoord2f(x2, y2);
			glVertex2f(x2c, y2c);
			glTexCoord2f(x2, y);
			glVertex2f(x2c, yc);

			glEnd();
			//**DEBUG START
			if(GameRenderer.debug){
				glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
				glBegin(org.lwjgl.opengl.GL11.GL_LINE_STRIP);

				int d_xc = p.getX();
				int d_yc = p.getY();
				int d_x2c = d_xc+p.getWidth();
				int d_y2c = d_yc+p.getHeight();

				glVertex2f(d_xc, d_yc);
				glVertex2f(d_x2c, d_yc);
				glVertex2f(d_x2c, d_y2c);
				glVertex2f(d_xc, d_y2c);
				glVertex2f(d_xc, d_yc);

				glEnd();

				glBegin(org.lwjgl.opengl.GL11.GL_LINE_STRIP);

				d_xc = p.getCollider().getX();
				d_yc = p.getCollider().getY();
				d_x2c = d_xc+p.getCollider().getWidth();
				d_y2c = d_yc+p.getCollider().getHeight();

				glVertex2f(d_xc, d_yc);
				glVertex2f(d_x2c, d_yc);
				glVertex2f(d_x2c, d_y2c);
				glVertex2f(d_xc, d_y2c);
				glVertex2f(d_xc, d_yc);

				glEnd();
				glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture);
			}
			//**DEBUG END
		}

		for(Projectile p : GameEngine.ennemyProjectiles){

			x = SpriteSheet.sprites.get(p.getAnim().getSprite()).getX();
			y = SpriteSheet.sprites.get(p.getAnim().getSprite()).getY();
			x2 = SpriteSheet.sprites.get(p.getAnim().getSprite()).getX() + SpriteSheet.sprites.get(p.getAnim().getSprite()).getWidth();
			y2 = SpriteSheet.sprites.get(p.getAnim().getSprite()).getY() + SpriteSheet.sprites.get(p.getAnim().getSprite()).getHeight();

			glBegin(GL_QUADS);

			xc = p.getX();
			yc = p.getY();
			x2c = p.getX()+p.getWidth();
			y2c = p.getY()+p.getHeight();

			glTexCoord2f(x, y);
			glVertex2f(xc, yc);
			glTexCoord2f(x, y2);
			glVertex2f(xc, y2c);
			glTexCoord2f(x2, y2);
			glVertex2f(x2c, y2c);
			glTexCoord2f(x2, y);
			glVertex2f(x2c, yc);

			glEnd();
		}

		for(Explosion e : GameEngine.explosions){

			x = SpriteSheet.sprites.get(e.getAnim().getSprite()).getX();
			y = SpriteSheet.sprites.get(e.getAnim().getSprite()).getY();
			x2 = x + SpriteSheet.sprites.get(e.getAnim().getSprite()).getWidth();
			y2 = y + SpriteSheet.sprites.get(e.getAnim().getSprite()).getHeight();

			glBegin(GL_QUADS);

			xc = e.getX();
			yc = e.getY();
			x2c = xc+e.getWidth();
			y2c = yc+e.getHeight();

			glTexCoord2f(x, y);
			glVertex2f(xc, yc);
			glTexCoord2f(x, y2);
			glVertex2f(xc, y2c);
			glTexCoord2f(x2, y2);
			glVertex2f(x2c, y2c);
			glTexCoord2f(x2, y);
			glVertex2f(x2c, yc);

			glEnd();
		}
	}

	@Override
	public void update() {

		// Player projectiles update
		for(Projectile p: GameEngine.player.projectiles){
			p.update();
		}

		// Remove destroyed player projectiles from player projectiles list
		for(Projectile p : GameEngine.destroyedPlayerProjectiles){
			GameEngine.player.projectiles.remove(p);
		}
		GameEngine.destroyedPlayerProjectiles.clear();
		// Ennemy projectiles update
		for(Projectile p : GameEngine.ennemyProjectiles){
			p.update();
		}

		// Remove destroyed ennemy projectiles from ennemy projectiles list
		for(Projectile p : GameEngine.destroyedEnnemyProjectiles){
			GameEngine.ennemyProjectiles.remove(p);
		}
		GameEngine.destroyedEnnemyProjectiles.clear();

		ArrayList<Explosion> explosionsWhichAreNoMore=new ArrayList<Explosion>();
		
		// Explosions update
		for(Explosion e: GameEngine.explosions){
			e.update();
			if(e.getAnim().isEnded()){
				explosionsWhichAreNoMore.add(e);
			}
		}
		
		// Remove ended explosions
		for(Explosion e:explosionsWhichAreNoMore){
			GameEngine.explosions.remove(e);
		}

	}

}
