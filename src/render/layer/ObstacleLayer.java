package render.layer;

import static org.lwjgl.opengl.ARBTextureRectangle.GL_TEXTURE_RECTANGLE_ARB;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL11.glVertex2f;

import core.GameEngine;
import core.objects.obstacles.Obstacle;
import core.objects.projectiles.Projectile;
import render.GameRenderer;
import res.SpriteSheet;

public class ObstacleLayer extends Layer{

	public ObstacleLayer(String texturePath) {
		super(texturePath);
	}

	@Override
	public void drawLayer() {

		glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture);
		for(Obstacle roid : GameEngine.obstacles){
			x = SpriteSheet.sprites.get(roid.getAnim().getSprite()).getX();
			y = SpriteSheet.sprites.get(roid.getAnim().getSprite()).getY();
			x2 = x + SpriteSheet.sprites.get(roid.getAnim().getSprite()).getWidth();
			y2 = y + SpriteSheet.sprites.get(roid.getAnim().getSprite()).getHeight();
			glMatrixMode(GL_MODELVIEW); 
			glPushMatrix();
			glTranslatef(roid.getX(),roid.getY(),0); 
			//glRotatef(roid.getRotationAngle(),0,0,1);

			glBegin(GL_QUADS);

			xc = 0;
			yc = 0;
			x2c = xc+roid.getWidth();
			y2c = yc+roid.getHeight();

			glTexCoord2f(x, y);
			glVertex2f(xc, yc);
			glTexCoord2f(x, y2);
			glVertex2f(xc, y2c);
			glTexCoord2f(x2, y2);
			glVertex2f(x2c, y2c);
			glTexCoord2f(x2, y);
			glVertex2f(x2c, yc);

			glEnd();
			glPopMatrix();
			if(GameRenderer.debug){
				glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
				glBegin(org.lwjgl.opengl.GL11.GL_LINE_STRIP);

				int d_xc = roid.getX();
				int d_yc = roid.getY();
				int d_x2c = d_xc+roid.getWidth();
				int d_y2c = d_yc+roid.getHeight();

				glVertex2f(d_xc, d_yc);
				glVertex2f(d_x2c, d_yc);
				glVertex2f(d_x2c, d_y2c);
				glVertex2f(d_xc, d_y2c);
				glVertex2f(d_xc, d_yc);

				glEnd();

				glBegin(org.lwjgl.opengl.GL11.GL_LINE_STRIP);

				d_xc = roid.getCollider().getX();
				d_yc = roid.getCollider().getY();
				d_x2c = d_xc+roid.getCollider().getWidth();
				d_y2c = d_yc+roid.getCollider().getHeight();

				glVertex2f(d_xc, d_yc);
				glVertex2f(d_x2c, d_yc);
				glVertex2f(d_x2c, d_y2c);
				glVertex2f(d_xc, d_y2c);
				glVertex2f(d_xc, d_yc);

				glEnd();
			}
		}

	}

	@Override
	public void update(){

		// Update obstacles
		for(Obstacle p : GameEngine.obstacles){
			p.update();
		}

		// Remove obstacles projectiles from obstacles list
		for(Obstacle p : GameEngine.destroyedObstacles){
			GameEngine.obstacles.remove(p);
		}
		GameEngine.destroyedObstacles.clear();
	}

}
