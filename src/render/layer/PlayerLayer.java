package render.layer;

import static org.lwjgl.opengl.ARBTextureRectangle.GL_TEXTURE_RECTANGLE_ARB;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;

import core.GameEngine;
import render.GameRenderer;
import res.Sprite;
import res.SpriteSheet;

public class PlayerLayer extends Layer{
	
	private Sprite currentPlayerSprite;

	public PlayerLayer(String texturePath) {
		super(texturePath);
		currentPlayerSprite = SpriteSheet.sprites.get("player-0");
	}

	public void update(){

		GameEngine.player.update();

	}

	@Override
	public void drawLayer() {
		
    //**DEBUG START
    if(GameRenderer.debug){
      glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
      glBegin(org.lwjgl.opengl.GL11.GL_LINE_STRIP);

      int d_xc = GameEngine.player.getX();
      int d_yc = GameEngine.player.getY();
      int d_x2c = d_xc+GameEngine.player.getWidth();
      int d_y2c = d_yc+GameEngine.player.getHeight();

      glVertex2f(d_xc, d_yc);
      glVertex2f(d_x2c, d_yc);
      glVertex2f(d_x2c, d_y2c);
      glVertex2f(d_xc, d_y2c);
      glVertex2f(d_xc, d_yc);

      glEnd();
    }
    //**DEBUG END
	  
		glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture);

		if(GameEngine.player.forward<2){

			x = SpriteSheet.sprites.get("particles-"+GameEngine.player.getParticlesState()).getX();
			y = SpriteSheet.sprites.get("particles-"+GameEngine.player.getParticlesState()).getY();
			x2 = SpriteSheet.sprites.get("particles-"+GameEngine.player.getParticlesState()).getX() + SpriteSheet.sprites.get("particles-"+GameEngine.player.getParticlesState()).getWidth();
			y2 = SpriteSheet.sprites.get("particles-"+GameEngine.player.getParticlesState()).getY() + SpriteSheet.sprites.get("particles-"+GameEngine.player.getParticlesState()).getHeight();

			glBegin(GL_QUADS);

			xc = GameEngine.player.getX();
			yc = GameEngine.player.getY()+GameEngine.player.getHeight();
			x2c = GameEngine.player.getX()+GameEngine.player.getWidth();
			y2c = GameEngine.player.getY()+GameEngine.player.getHeight()+GameEngine.player.getHeight()/2;

			glTexCoord2f(x, y);
			glVertex2f(xc, yc);
			glTexCoord2f(x, y2);
			glVertex2f(xc, y2c);
			glTexCoord2f(x2, y2);
			glVertex2f(x2c, y2c);
			glTexCoord2f(x2, y);
			glVertex2f(x2c, yc);

			glEnd();

			if(GameEngine.player.forward==1){

				x = SpriteSheet.sprites.get("reactor-"+GameEngine.player.getReactorState()).getX();
				y = SpriteSheet.sprites.get("reactor-"+GameEngine.player.getReactorState()).getY();
				x2 = SpriteSheet.sprites.get("reactor-"+GameEngine.player.getReactorState()).getX() + SpriteSheet.sprites.get("reactor-"+GameEngine.player.getReactorState()).getWidth();
				y2 = SpriteSheet.sprites.get("reactor-"+GameEngine.player.getReactorState()).getY() + SpriteSheet.sprites.get("reactor-"+GameEngine.player.getReactorState()).getHeight();

				glBegin(GL_QUADS);

				xc = GameEngine.player.getX();
				yc = GameEngine.player.getY()+GameEngine.player.getHeight();
				x2c = GameEngine.player.getX()+GameEngine.player.getWidth();
				y2c = GameEngine.player.getY()+GameEngine.player.getHeight()+GameEngine.player.getHeight()/2;

				glTexCoord2f(x, y);
				glVertex2f(xc, yc);
				glTexCoord2f(x, y2);
				glVertex2f(xc, y2c);
				glTexCoord2f(x2, y2);
				glVertex2f(x2c, y2c);
				glTexCoord2f(x2, y);
				glVertex2f(x2c, yc);

				glEnd();

			}

		}

		x = currentPlayerSprite.getX();
		y = currentPlayerSprite.getY();
		x2 = currentPlayerSprite.getX() + currentPlayerSprite.getWidth();
		y2 = currentPlayerSprite.getY() + currentPlayerSprite.getHeight();

		glBegin(GL_QUADS);

		xc = GameEngine.player.getX();
		yc = GameEngine.player.getY();
		x2c = GameEngine.player.getX()+GameEngine.player.getWidth();
		y2c = GameEngine.player.getY()+GameEngine.player.getHeight();

		glTexCoord2f(x, y);
		glVertex2f(xc, yc);
		glTexCoord2f(x, y2);
		glVertex2f(xc, y2c);
		glTexCoord2f(x2, y2);
		glVertex2f(x2c, y2c);
		glTexCoord2f(x2, y);
		glVertex2f(x2c, yc);

		glEnd();
		
    //**DEBUG START
    if(GameRenderer.debug){
      glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
      glBegin(org.lwjgl.opengl.GL11.GL_LINE_STRIP);

      int d_xc = GameEngine.player.getCollider().getX();
      int d_yc = GameEngine.player.getCollider().getY();
      int d_x2c = d_xc+GameEngine.player.getCollider().getWidth();
      int d_y2c = d_yc+GameEngine.player.getCollider().getHeight();

      glVertex2f(d_xc, d_yc);
      glVertex2f(d_x2c, d_yc);
      glVertex2f(d_x2c, d_y2c);
      glVertex2f(d_xc, d_y2c);
      glVertex2f(d_xc, d_yc);

      glEnd();
    }
    //**DEBUG END
		
	}
	
	public void setCurrentPlayerSprite(int direction){
		currentPlayerSprite = SpriteSheet.sprites.get("player-"+direction);
	}
}
