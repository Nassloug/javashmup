package render.layer;

public abstract class Layer {

	public String texturePath;
	public int texture;
	public int x,y,x2,y2,xc,yc,x2c,y2c;
	
	public Layer(String texturePath){
		this.texturePath = texturePath;
		 x=y=x2=y2=xc=yc=x2c=y2c=0;
	}
	
	public abstract void drawLayer();
	
	public abstract void update();
	
}
