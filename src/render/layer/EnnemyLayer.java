package render.layer;

import static org.lwjgl.opengl.ARBTextureRectangle.GL_TEXTURE_RECTANGLE_ARB;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;

import core.GameEngine;
import core.objects.Module;
import core.objects.ennemies.Ennemy;
import res.SpriteSheet;

public class EnnemyLayer extends Layer{

	public EnnemyLayer(String texturePath) {
		super(texturePath);
	}

	@Override
	public void drawLayer() {
		
		glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture);
		for(Ennemy e: GameEngine.ennemies){

			x = SpriteSheet.sprites.get(e.getAnim().getSprite()).getX();
			y = SpriteSheet.sprites.get(e.getAnim().getSprite()).getY();
			x2 = SpriteSheet.sprites.get(e.getAnim().getSprite()).getX() + SpriteSheet.sprites.get(e.getAnim().getSprite()).getWidth();
			y2 = SpriteSheet.sprites.get(e.getAnim().getSprite()).getY() + SpriteSheet.sprites.get(e.getAnim().getSprite()).getHeight();

			glBegin(GL_QUADS);

			xc = e.getX();
			yc = e.getY();
			x2c = e.getX()+e.getWidth();
			y2c = e.getY()+e.getHeight();

			glTexCoord2f(x, y);
			glVertex2f(xc, yc);
			glTexCoord2f(x, y2);
			glVertex2f(xc, y2c);
			glTexCoord2f(x2, y2);
			glVertex2f(x2c, y2c);
			glTexCoord2f(x2, y);
			glVertex2f(x2c, yc);

			glEnd();

			for(Module m: e.getModules()){
				if(m.isVisible()){

					x = SpriteSheet.sprites.get(m.getAnim().getSprite()).getX();
					y = SpriteSheet.sprites.get(m.getAnim().getSprite()).getY();
					x2 = SpriteSheet.sprites.get(m.getAnim().getSprite()).getX() + SpriteSheet.sprites.get(m.getAnim().getSprite()).getWidth();
					y2 = SpriteSheet.sprites.get(m.getAnim().getSprite()).getY() + SpriteSheet.sprites.get(m.getAnim().getSprite()).getHeight();

					glBegin(GL_QUADS);

					xc = m.getX();
					yc = m.getY();
					x2c = m.getX()+m.getWidth();
					y2c = m.getY()+m.getHeight();

					glTexCoord2f(x, y);
					glVertex2f(xc, yc);
					glTexCoord2f(x, y2);
					glVertex2f(xc, y2c);
					glTexCoord2f(x2, y2);
					glVertex2f(x2c, y2c);
					glTexCoord2f(x2, y);
					glVertex2f(x2c, yc);

					glEnd();

				}
			}
		}
		
	}

	@Override
	public void update() {
		
		// Ennemies update
		for(Ennemy e : GameEngine.ennemies){
			e.update();
		}

		// Remove destroyed ennemies from ennemies list
		for(Ennemy e : GameEngine.destroyedEnnemies){
			GameEngine.ennemies.remove(e);
		}
		GameEngine.destroyedEnnemies.clear();
		
	}

}
