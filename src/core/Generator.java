package core;

import java.util.ArrayList;
import java.util.List;

import core.patterns.Pattern;

public abstract class Generator {
	
	  public List<Pattern> generator;
	  
	  public Generator(){
		  generator = new ArrayList<Pattern>();
	  }

	  public void addPattern(Pattern pattern){
		  generator.add(pattern);
	  }
	  
	  public void update(){
		  
	  }
}
