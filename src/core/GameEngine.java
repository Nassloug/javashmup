package core;

import java.util.ArrayList;
import java.util.List;

import core.levels.MasterUI;
import core.objects.Explosion;
import core.objects.Player;
import core.objects.TextPlate;
import core.objects.ennemies.Ennemy;
import core.objects.obstacles.Obstacle;
import core.objects.projectiles.Projectile;
import input.ControllerHandler;
import render.GameRenderer;
import render.gui.TitleScreen;
import sound.SoundPlayer;

public class GameEngine {

	public static Player player;
	public static Generator generator;
	public static List<Ennemy> ennemies;
	public static List<Ennemy> destroyedEnnemies;
	public static List<Explosion> explosions;
	public static List<Obstacle> obstacles;
	public static List<Projectile> ennemyProjectiles;
	public static List<Projectile> destroyedPlayerProjectiles;
	public static List<Projectile> destroyedEnnemyProjectiles;
	public static List<Obstacle> destroyedObstacles;
	public static List<TextPlate> textPlates;
	public static int winW = 640, winH = 1280;
	public static SoundPlayer sp;

	public static void main(String args[]){
		ennemies = new ArrayList<Ennemy>();
		destroyedEnnemies = new ArrayList<Ennemy>();
		ennemyProjectiles = new ArrayList<Projectile>();
		obstacles = new ArrayList<Obstacle>();
		explosions = new ArrayList<Explosion>();
		destroyedPlayerProjectiles = new ArrayList<Projectile>();
		destroyedEnnemyProjectiles = new ArrayList<Projectile>();
		destroyedObstacles = new ArrayList<Obstacle>();
		textPlates = new ArrayList<TextPlate>();
		GameRenderer.setup();
		GameRenderer.loadLevel(new MasterUI(new TitleScreen()));
		sp = new SoundPlayer();
		sp.addSound("themes/Clouds Mission.wav","level00");
		sp.setVolumeSound("level00", -10f);
		sp.addSound("piou.wav","projectile_launch_1");
		sp.setVolumeSound("projectile_launch_1", -20f);
		sp.addSound("pow.wav","projectile_launch_2");
		sp.addSound("missing_sound.wav","missing_sound");
		sp.setVolumeSound("missing_sound", -30f);
		sp.addSound("themes/rus.wav","level01");
		sp.setVolumeSound("level01", -10f);
		//new Background(GameEngine.winW,GameEngine.winH,10,2,15,0,0, new String[]{"background-0"}, new String[]{"background-1","background-2","background-3","background-4","background-5","background-6","background-7","background-8"}, new String[]{"background-9"}, spriteSheetTexture[2]);

		/*Ennemy eminem=new Ennemy(winW/2-32,winH/2-32,64,64,new Collider(5+winW/2-32,5,54,54),40,null,new String[]{"ennemy-0","ennemy-1"});
		ennemies.add(eminem);*/
		GameRenderer.run();
	}

	public static void update(){
		ControllerHandler.updateControllerState();
		GameRenderer.level.update();
		if(generator!=null){
			generator.update();
		}
	}

}
