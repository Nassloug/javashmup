package core;

import core.objects.GameObject;

public class Collider {

	private int x,y,width,height,deltaX,deltaY;
	
	public Collider(int x, int y, int width, int height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public boolean doesCollide(Collider other){
		if(other.x>x+width){
			return false;
		}else if(other.x+other.width<x){
			return false;
		}else if(other.y>y+height){
			return false;
		}else if(other.y+other.height<y){
			return false;
		}else{
			return true;
		}
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public void initDelta(GameObject gameObject){
		if(gameObject!=null){
			deltaX = x-gameObject.getX();
			deltaY = y-gameObject.getY();;
		}
	}
	
	public void moveWithObject(GameObject gameObject){
		x = gameObject.getX() + deltaX;
		y = gameObject.getY() + deltaY;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
}
