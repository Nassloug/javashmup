package core.objects;

import core.Collider;
import core.GameEngine;
import render.Animation;

public class Explosion extends GameObject{

	private int power;

	public Explosion(int x, int y, int width, int height, Collider collider, Animation anim, int power){
		super(x,y,width,height,collider,anim);
		anim.setAnimWidth(width);
		anim.setAnimHeight(height);
		this.power=power;
		GameEngine.sp.playSound("boomq");
	}

	public void update(){
		anim.animate(0,false,false,0.5);
		super.update();
	}

	public void setPower(int power){
		this.power = power;
	}

	public int getPower(){
		return power;
	}

}
