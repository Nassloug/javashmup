package core.objects;

import java.util.ArrayList;
import java.util.List;

import core.Collider;
import core.GameEngine;
import core.trajectories.Trajectory;
import render.Animation;

//Abstract class for every interactive objects
public abstract class GameObject {

	public final float inertiaGain=0.1f,inertiaLoss=0.025f;
	protected int x,y,width,height,newX,newY;
	protected boolean resistBoundaries;
	protected float xInertia,yInertia,
	/**center of projectile first appearance
	 *in %, relative to GameObject rectangle
	 *centered by default**/
	fireHoleX=1/2,fireHoleY=1/2;
	protected Collider collider;
	protected Animation anim;
	protected List<Trajectory> trajectories;

	protected boolean reinitCollider;

	public GameObject(int x, int y, int width, int height, Collider collider, Animation anim){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.collider = collider;
		if(collider!=null){
			this.collider.initDelta(this);
		}
		this.anim = anim;
		trajectories = new ArrayList<Trajectory>();
		resistBoundaries = false;
		reinitCollider = false;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Collider getCollider() {
		return collider;
	}

	public void setCollider(Collider collider) {
		this.collider = collider;
		if(collider!=null){
			this.collider.initDelta(this);
		}
	}

	public void setAnim(Animation anim){
		this.anim = anim;
	}

	public Animation getAnim(){
		return anim;
	}
	
	public void setTrajectories(List<Trajectory> trajectories) {
		this.trajectories = trajectories;
	}
	
	public List<Trajectory> getTrajectories(){
		return trajectories;
	}
	
	public void addTrajectory(Trajectory trajectory){
		trajectories.add(trajectory);
	}

	public void setFireHole(float x,float y){
		fireHoleX=x;
		fireHoleY=y;
	}

	public float getFireHoleX(){
		return fireHoleX;
	}

	public float getFireHoleY(){
		return fireHoleY;
	}

	public void update(){
		if(!resistBoundaries && (getX()>GameEngine.winW || getX()+getWidth()<0 || getY()>GameEngine.winH || getY()+getHeight()<0)){
			destroy();
		}
	}

	public void destroy(){
		//System.out.println("destroyed :" + this);
	}

	public void followTrajectory(){
		if(trajectories!=null&&!trajectories.isEmpty()){
			newX=0;
			newY=0;
			for(Trajectory trajectory : trajectories){
				newX+=trajectory.nextX();
				newY+=trajectory.nextY();
				/*collider.setX(collider.getX()+newX);
				collider.setY(collider.getY()+newY);*/
				trajectory.update();
			}
			x=newX+trajectories.get(0).getX();
			y=newY+trajectories.get(0).getY();
			if(collider!=null){
				collider.moveWithObject(this);
			}
		}
	}

	public void move(int dx,int dy,boolean inertia){
		if(inertia){
			//annule l'inertie si elle est contraire au mouvement
			if(yInertia<0&&dy>0)setyInertia(0);
			if(yInertia>0&&dy<0)setyInertia(0);
			if(xInertia<0&&dx>0)setxInertia(0);
			if(xInertia>0&&dx<0)setxInertia(0);
			//accelere
			setyInertia(yInertia+Integer.signum(dy)*inertiaGain);
			setxInertia(xInertia+Integer.signum(dx)*inertiaGain);
		}
		dx+=xInertia;
		dy+=yInertia;
		//freine
		if(Math.abs(yInertia)<0.01f){
			setyInertia(0);
		}else{
			setyInertia(yInertia-Math.signum(yInertia)*inertiaLoss);
		}
		if(Math.abs(xInertia)<0.01f){
			setxInertia(0);
		}else{
			setxInertia(xInertia-Math.signum(xInertia)*inertiaLoss);
		}
		y+=dy;
		x+=dx;
		collider.setY(collider.getY()+dy);
		collider.setX(collider.getX()+dx);
	}

	public float getxInertia() {
		return xInertia;
	}

	public void setxInertia(float xInertia) {
		if(xInertia>1)xInertia=1;
		if(xInertia<-1)xInertia=-1;
		this.xInertia = xInertia;
	}

	public float getyInertia() {
		return yInertia;
	}

	public void setyInertia(float yInertia) {
		if(yInertia>1)yInertia=1;
		if(yInertia<-1)yInertia=-1;
		this.yInertia = yInertia;
	}

	public boolean doesCollide(GameObject other){
		return(getCollider().doesCollide(other.getCollider()));
	}

	public boolean doesCollide(Collider other){
		return(getCollider().doesCollide(other));
	}

	public void resistBoundaries(){
		resistBoundaries = true;
	}
}
