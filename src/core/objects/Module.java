package core.objects;

import java.util.ArrayList;
import java.util.List;

import core.Collider;
import core.GameEngine;
import core.objects.ennemies.Ennemy;
import core.objects.projectiles.Projectile;
import core.objects.projectiles.ProjectileType;
import core.patterns.Pattern;
import core.trajectories.Trajectory;
import render.Animation;

public class Module extends GameObject{

	private List<Pattern> patterns;
	private Ennemy ennemy;
	private boolean destroyable;
	private boolean visible;
	private int health;

	public Module(int x, int y, int width, int height, Collider collider, Animation anim, List<Trajectory> trajectories, int health, Ennemy ennemy) {
		super(x, y, width, height, collider,anim);
		if(anim!=null){
			visible = true;
		}else{
			visible = false;
		}
		if(health>0){
			destroyable = true;
			this.health = health;
		}else{
			destroyable = false;
			this.health = 1;
		}
		this.trajectories = trajectories;
		this.ennemy = ennemy;
		this.patterns = new ArrayList<Pattern>();
	}
	
	public void update(){
		if(getCollider()!=null){
			for(Projectile p : GameEngine.player.projectiles){
				if(p.doesCollide(getCollider())){
					if(destroyable){
						health-=p.getPower();
					}
					if(ProjectileType.DEFAULT.equals(p.getType())){
						p.explode();
					}
					anim.startAnim();
					anim.animate(1,false,true,1);
				}
			}
		}
		if(health<=0){
			destroy();
		}
		followTrajectory();
		if(visible){
			anim.animate(0,true,false,1);
		}
		for(Pattern p : patterns){
			p.initFire();
		}
		super.update();
	}
	
	public void addPattern(Pattern pattern){
		patterns.add(pattern);
	}
	
	public void removePattern(int idx){
		patterns.remove(idx);
	}
	
	public Ennemy getEnnemy() {
		return ennemy;
	}

	public void setEnnemy(Ennemy ennemy) {
		this.ennemy = ennemy;
	}

	public boolean isDestroyable() {
		return destroyable;
	}

	public void setDestroyable(boolean destroyable) {
		this.destroyable = destroyable;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		if(anim!=null){
			this.visible = visible;
		}
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	@Override
	public void destroy(){
		ennemy.destroyModule(this);
		super.destroy();
	}
}
