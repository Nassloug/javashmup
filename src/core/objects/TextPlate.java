package core.objects;

public class TextPlate{
	private String text;
	private GameObject host;
	private int xOffSet, yOffSet;
	private float spacing;

	public TextPlate(GameObject host, int xOffSet, int yOffSet, float spacing, String text) {
		this.setHost(host);
		this.setText(text);
		this.setxOffSet(xOffSet);
		this.setyOffSet(yOffSet);
		this.setSpacing(spacing);
	}

	public String getText(){
		return text;
	}

	public void setText(String text){
		this.text=text;
	}

	public GameObject getHost() {
		return host;
	}

	public void setHost(GameObject host) {
		this.host = host;
	}

	public int getxOffSet() {
		return xOffSet;
	}

	public void setxOffSet(int xOffSet) {
		this.xOffSet = xOffSet;
	}

	public int getyOffSet() {
		return yOffSet;
	}

	public void setyOffSet(int yOffSet) {
		this.yOffSet = yOffSet;
	}

	public int getX() {
		return host.getX()+host.getWidth()/2+xOffSet;
	}

	public int getY() {
		return host.getY()+host.getHeight()/2+yOffSet;
	}

	public float getSpacing() {
		return spacing;
	}

	public void setSpacing(float spacing) {
		this.spacing = spacing;
	}

}
