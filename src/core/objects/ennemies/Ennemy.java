package core.objects.ennemies;

import java.util.ArrayList;
import java.util.List;

import core.Collider;
import core.GameEngine;
import core.objects.Explosion;
import core.objects.GameObject;
import core.objects.Module;
import core.objects.projectiles.Projectile;
import core.objects.projectiles.ProjectileType;
import core.patterns.Pattern;
import core.trajectories.Trajectory;
import render.Animation;

public class Ennemy extends GameObject{

	protected int health;
	protected List<Pattern> patterns;
	protected List<Module> modules;
	protected List<Module> destroyedModules;

	public Ennemy(int x, int y, int width, int height, Collider collider, int health, List<Trajectory> trajectories, Animation anim) {
		super(x, y, width, height, collider, anim);
		this.health = health;
		this.trajectories=trajectories;
		anim.stopAnim();
		patterns = new ArrayList<Pattern>();
		modules = new ArrayList<Module>();
		destroyedModules = new ArrayList<Module>();
	}

	public void update(){
		for(Projectile p : GameEngine.player.projectiles){
			if(p.doesCollide(getCollider())){
				health-=p.getPower();
				if(ProjectileType.DEFAULT.equals(p.getType())){
					p.explode();
				}
				anim.startAnim();
				anim.animate(1,false,true,1);
			}
		}
		if(health<=0){
			destroy();
		}
		followTrajectory();
		//new Aiming(this,GameEngine.player);
		anim.animate(1,false,true,1);
		for(Pattern p : patterns){
			p.initFire();
		}
		for(Module m : modules){
			m.update();
		}
		for(Module m : destroyedModules){
			modules.remove(m);
		}
		destroyedModules.clear();
		super.update();
	}

	public void addModule(Module module){
		modules.add(module);
	}

	public void destroyModule(Module module){
		destroyedModules.add(module);
	}

	public void addPattern(Pattern pattern){
		patterns.add(pattern);
	}

	public void removePattern(int idx){
		patterns.remove(idx);
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(ArrayList<Module> modules) {
		this.modules = modules;
	}

	public List<Module> getDestroyedModules() {
		return destroyedModules;
	}

	public void setDestroyedModules(ArrayList<Module> destroyedModules) {
		this.destroyedModules = destroyedModules;
	}

	@Override
	public void destroy(){
		GameEngine.destroyedEnnemies.add(this);
		super.destroy();
	}

}
