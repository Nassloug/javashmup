package core.objects.ennemies;

import java.util.ArrayList;

import core.Collider;
import core.GameEngine;
import core.objects.GameObject;
import core.objects.Module;
import core.objects.projectiles.Projectile;
import core.objects.projectiles.ProjectileType;
import core.patterns.DefaultPattern;
import core.patterns.Nova;
import core.patterns.Pattern;
import core.trajectories.Linear;
import core.trajectories.Trajectory;
import render.Animation;

public class Boss00 extends Ennemy{

	private Collider[] colliders;
	private int moveSpeed;
	
	public Boss00() {
		super(0, 0, 320+64, 320+64, null, 1000, null, new Animation(new String[]{"boss"}));
		colliders = new Collider[]{new Collider(x,y,width,height)};
		for(Collider c : colliders){
			c.initDelta(this);
		}
		Trajectory trajectory = new Linear(x,y,moveSpeed,1,0);
		trajectories = new ArrayList<Trajectory>();
		trajectories.add(trajectory);
		resistBoundaries = true;
		moveSpeed = 20;
		// Modules initialization
		
		Module reactor = new Module(x+64*3, y+4*64+2*32+16+16, 0, 0, null, null, null, 0, this);
		reactor.resistBoundaries();
		Trajectory reactorTrajectory = new Linear(reactor.getX(),reactor.getY(),moveSpeed,1,0);
		ArrayList<Trajectory> reactorTrajectories = new ArrayList<Trajectory>();
		reactorTrajectories.add(reactorTrajectory);
		reactor.setTrajectories(reactorTrajectories);
		Pattern reactorPattern = new Nova(reactor, 1, 800, 0.5f, 2, -100, true);
		reactorPattern.setAnimation(new Animation(new String[]{"ennemy-projectile-0","ennemy-projectile-1","ennemy-projectile-2","ennemy-projectile-3"}));
		reactor.addPattern(reactorPattern);
		addModule(reactor);
		/*
		Module cannon1bis = new Module(x+64+32+16, y+3*64+32+16+16, 0, 0, null, null, Arrays.asList(new Graviton(x+64+32+16,y+3*64+32+16+16,-10,30,30)), 0, this);
		Pattern cannon1bisPattern = new DefaultPattern(cannon1bis, 1, 10, 300, -500, true);
		//cannon1bisPattern.setSprites(new String[]{"boss-projectile-1"});
		cannon1.addPattern(cannon1bisPattern);
		addModule(cannon1bis);
		*/
		Module cannon1plus = new Module(x+64+32+16, y+3*64+2*32+2*16+10, 0, 0, null, null, null, 0, this);
		cannon1plus.resistBoundaries();
		Trajectory cannon1Trajectory = new Linear(cannon1plus.getX(),cannon1plus.getY(),moveSpeed,1,0);
		ArrayList<Trajectory> cannon1Trajectories = new ArrayList<Trajectory>();
		cannon1Trajectories.add(cannon1Trajectory);
		cannon1plus.setTrajectories(cannon1Trajectories);
		Pattern cannon1plusPattern = new DefaultPattern(cannon1plus, 1, 150, 1, 50, -500, true);
		cannon1plusPattern.setAnimation(new Animation(new String[]{"boss-projectile-0","boss-projectile-1","boss-projectile-2","boss-projectile-3"}));
		cannon1plus.addPattern(cannon1plusPattern);
		addModule(cannon1plus);
		/*
		Module cannon2 = new Module(x+width-64-32-16, y+3*64+32+16+16, 0, 0, null, null, Arrays.asList(new Graviton(x+width-64-32-16,y+3*64+32+16+16,10,30,30)), 0, this);
		Pattern cannon2Pattern = new DefaultPattern(cannon2, 1, 10, 300, -500, true);
		//cannon2Pattern.setSprites(new String[]{"boss-projectile-1"});
		cannon2.addPattern(cannon2Pattern);
		addModule(cannon2);
		
		Module cannon2bis = new Module(x+width-64-32-16, y+3*64+32+16+16, 0, 0, null, null, Arrays.asList(new Graviton(x+width-64-32-16,y+3*64+32+16+16,-10,30,30)), 0, this);
		Pattern cannon2bisPattern = new DefaultPattern(cannon2bis, 1, 10, 300, -500, true);
		//cannon2bisPattern.setSprites(new String[]{"boss-projectile-1"});
		cannon2.addPattern(cannon2bisPattern);
		addModule(cannon2bis);
		*/
		Module cannon2plus = new Module(x+width-64-32-16, y+3*64+2*32+2*16+10, 0, 0, null, null, null, 0, this);
		cannon2plus.resistBoundaries();
		Trajectory cannon2Trajectory = new Linear(cannon2plus.getX(),cannon2plus.getY(),moveSpeed,1,0);
		ArrayList<Trajectory> cannon2Trajectories = new ArrayList<Trajectory>();
		cannon2Trajectories.add(cannon2Trajectory);
		cannon2plus.setTrajectories(cannon2Trajectories);
		Pattern cannon2plusPattern = new DefaultPattern(cannon2plus, 1, 150, 1, 50, -500, true);
		cannon2plusPattern.setAnimation(new Animation(new String[]{"boss-projectile-0","boss-projectile-1","boss-projectile-2","boss-projectile-3"}));
		cannon2plus.addPattern(cannon2plusPattern);
		addModule(cannon2plus);
	}
	
	@Override
	public boolean doesCollide(Collider other){
		for(Collider collider : colliders){
			if(other.doesCollide(collider)){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean doesCollide(GameObject other){
		for(Collider collider : colliders){
			if(other.doesCollide(collider)){
				return true;
			}
		}
		return false;
	}
	
	private void refreshColliders(){
		for(Collider c : colliders){
			c.moveWithObject(this);
		}
	}
	
	@Override
	public void update(){
		for(Projectile p : GameEngine.player.projectiles){
			if(doesCollide(p)){
				//health-=p.getPower();
				if(ProjectileType.DEFAULT.equals(p.getType())){
					p.explode();
				}
				anim.startAnim();
				anim.animate(1,false,true,1);
			}
		}
		if(health<=0){
			destroy();
		}
		if(x>=640-width){
			Trajectory trajectory = new Linear(x-1,y,-moveSpeed,1,0);
			trajectories.clear();
			trajectories.add(trajectory);
			
			Trajectory reactorTrajectory = new Linear(modules.get(0).getX()-1,modules.get(0).getY(),-moveSpeed,1,0);
			ArrayList<Trajectory> reactorTrajectories = new ArrayList<Trajectory>();
			reactorTrajectories.add(reactorTrajectory);
			modules.get(0).setTrajectories(reactorTrajectories);
			
			Trajectory cannon1Trajectory = new Linear(modules.get(1).getX()-1,modules.get(1).getY(),-moveSpeed,1,0);
			ArrayList<Trajectory> cannon1Trajectories = new ArrayList<Trajectory>();
			cannon1Trajectories.add(cannon1Trajectory);
			modules.get(1).setTrajectories(cannon1Trajectories);
			
			Trajectory cannon2Trajectory = new Linear(modules.get(2).getX()-1,modules.get(2).getY(),-moveSpeed,1,0);
			ArrayList<Trajectory> cannon2Trajectories = new ArrayList<Trajectory>();
			cannon2Trajectories.add(cannon2Trajectory);
			modules.get(2).setTrajectories(cannon2Trajectories);
		}else if(x<=0){
			Trajectory trajectory = new Linear(1,y,moveSpeed,1,0);
			trajectories.clear();
			trajectories.add(trajectory);
			
			Trajectory reactorTrajectory = new Linear(modules.get(0).getX()+1,modules.get(0).getY(),moveSpeed,1,0);
			ArrayList<Trajectory> reactorTrajectories = new ArrayList<Trajectory>();
			reactorTrajectories.add(reactorTrajectory);
			modules.get(0).setTrajectories(reactorTrajectories);
			
			Trajectory cannon1Trajectory = new Linear(modules.get(1).getX()+1,modules.get(1).getY(),moveSpeed,1,0);
			ArrayList<Trajectory> cannon1Trajectories = new ArrayList<Trajectory>();
			cannon1Trajectories.add(cannon1Trajectory);
			modules.get(1).setTrajectories(cannon1Trajectories);
			
			Trajectory cannon2Trajectory = new Linear(modules.get(2).getX()+1,modules.get(2).getY(),moveSpeed,1,0);
			ArrayList<Trajectory> cannon2Trajectories = new ArrayList<Trajectory>();
			cannon2Trajectories.add(cannon2Trajectory);
			modules.get(2).setTrajectories(cannon2Trajectories);
		}
		followTrajectory();
		//new Aiming(this,GameEngine.player);
		anim.animate(1,false,true,1);
		for(Pattern p : patterns){
			p.initFire();
		}
		for(Module m : modules){
			m.update();
		}
		for(Module m : destroyedModules){
			modules.remove(m);
		}
		destroyedModules.clear();
		refreshColliders();
	}

}
