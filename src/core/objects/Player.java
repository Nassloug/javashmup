package core.objects;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;

import java.util.ArrayList;

import org.lwjgl.glfw.GLFW;

import core.Collider;
import core.GameEngine;
import core.objects.projectiles.Projectile;
import core.objects.projectiles.ProjectileType;
import core.patterns.Pattern;
import input.ControllerHandler;
import input.KeyboardHandler;
import input.MouseHandler;
import render.GameRenderer;
import render.layer.PlayerLayer;

public class Player extends GameObject{

	public int direction = 0; //0 = straight, 1 = left, 2 = right
	public int forward = 0; //0 = straight, 1 = forward, 2 = backward
	public ArrayList<Projectile> projectiles;
	private Pattern[] patterns;
	private float speed;
	private int health;
	private int reactorState, particlesState, reactorTimer, particlesTimer;
	public PlayerLayer layer;

	public Player(int x, int y, int width, int height,int health, float speed, Collider collider, PlayerLayer layer){
		super(x,y,width,height,collider,null);
		setFireHole(1/2f,-1/4f);
		this.health = health;
		this.speed = (float)(GameEngine.winH)*speed/(100f*GameRenderer.REFRESH_RATE);
		projectiles = new ArrayList<Projectile>();
		patterns = new Pattern[2];
		reactorState = particlesState = 0;
		this.layer = layer;
	}

	public void update(){
		boolean moved=false;
		forward = 0;

		for(Projectile p : GameEngine.ennemyProjectiles){
			if(p.doesCollide(this)){
				if(ProjectileType.DESTROYABLE.equals(p.getType())){
					p.explode();
				}
				damage(p.getPower());
			}
		}
		if((KeyboardHandler.isKeyDown(GLFW_KEY_A)||ControllerHandler.dx<-ControllerHandler.CONTROLLER_TRESHOLD)&&getX()>0){
			//System.out.println("GLFW_KEY_Q");
			move(-(int)speed,0,false);
			moved=true;
			if(direction==0){
				direction=1;
			}else if(direction==2){
				direction=0;
			}
		}else if((KeyboardHandler.isKeyDown(GLFW_KEY_D)||ControllerHandler.dx>ControllerHandler.CONTROLLER_TRESHOLD)&&getX()+getWidth()<GameEngine.winW){
			//System.out.println("GLFW_KEY_D");
			move((int)speed,0,false);
			moved=true;
			if(direction==0){
				direction=2;
			}else if(direction==1){
				direction=0;
			}
		}else if(direction!=0){
			direction=0;			
		}
		if((KeyboardHandler.isKeyDown(GLFW_KEY_W)||ControllerHandler.dy<-ControllerHandler.CONTROLLER_TRESHOLD)&&getY()>0){
			//System.out.println("GLFW_KEY_Z");
			move(0,-(int)speed,false);
			moved=true;
			forward = 1;
		}else if((KeyboardHandler.isKeyDown(GLFW_KEY_S)||ControllerHandler.dy>ControllerHandler.CONTROLLER_TRESHOLD)&&getY()+getHeight()<GameEngine.winH){
			//System.out.println("GLFW_KEY_S");
			move(0,(int)speed,false);
			moved=true;
			forward = 2;
		}
		if(!moved)idle();
		layer.setCurrentPlayerSprite(direction);
		if(MouseHandler.isButtonDown(GLFW.GLFW_MOUSE_BUTTON_LEFT)||ControllerHandler.a){
			//System.out.println("GLFW_MOUSE_BUTTON_LEFT");
			if(patterns[1]!=null){
				patterns[1].stopFire();
			}
			if(patterns[0]!=null){
				patterns[0].resumeFire();
				patterns[0].initFire();
			}
		}else if(MouseHandler.isButtonDown(GLFW.GLFW_MOUSE_BUTTON_RIGHT)||ControllerHandler.b){
			//System.out.println("GLFW_MOUSE_BUTTON_RIGHT");
			if(patterns[0]!=null){
				patterns[0].stopFire();
			}
			if(patterns[1]!=null){
				patterns[1].resumeFire();
				patterns[1].initFire();
			}
		}else{
			patterns[1].stopFire();
			patterns[0].stopFire();
		}
		if(forward<2){
			particlesTimer++;
			if(particlesTimer>=GameRenderer.ANIM_RATE){
				if(particlesState<2){
					particlesState++;
				}else{
					particlesState = 0;
				}
				particlesTimer = 0;
			}
			if(forward==1){
				reactorTimer++;
				if(reactorTimer>=GameRenderer.ANIM_RATE){
					if(reactorState<2){
						reactorState++;
					}else{
						reactorState = 0;
					}
					reactorTimer = 0;
				}
			}
		}else{
			reactorState = particlesState = reactorTimer = particlesTimer = 0;
		}
	}

	//player is still
	public void idle(){
		if(getxInertia()!=0&&(getCollider().getX()+getCollider().getWidth()<GameEngine.winW&&getCollider().getX()>0))move((int)(getxInertia()*speed),0,false);
		if(getyInertia()!=0&&(getCollider().getY()+getCollider().getHeight()<GameEngine.winH&&getCollider().getY()>0))move(0,(int)(getyInertia()*speed),false);
	}

	public void damage(int power){
		health-=power;
		if(health<=0){
			destroy();
		}
	}

	public void setPattern(int idx, Pattern pattern){
		patterns[idx] = pattern;
	}

	public Pattern getPattern(int idx){
		return patterns[idx];
	}

	public void setSpeed(float speed){
		this.speed = speed;
	}

	public float getSpeed(){
		return speed;
	}

	public int getReactorState() {
		return reactorState;
	}

	public void setReactorState(int reactorState) {
		this.reactorState = reactorState;
	}

	public int getParticlesState() {
		return particlesState;
	}

	public void setParticlesState(int particlesState) {
		this.particlesState = particlesState;
	}

	public void removeProjectile(Projectile p){
		projectiles.remove(p);
	}

}
