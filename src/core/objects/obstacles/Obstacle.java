package core.objects.obstacles;

import core.Collider;
import core.GameEngine;
import core.objects.Explosion;
import core.objects.GameObject;
import core.objects.projectiles.Projectile;
import core.objects.projectiles.ProjectileType;
import render.Animation;

public class Obstacle extends GameObject{

	private int health, rotationAngle=0,
			rotationSpeed;//deg/s

	public Obstacle(int x, int y, int width, int height, Collider collider, Animation anim, int health, int power, boolean ennemy) {
		super(x, y, width, height, collider, anim);
		this.health = health;
	}

	public void update(){
		rotationAngle+=rotationSpeed;
		for(Projectile pp:GameEngine.player.projectiles){
			if(pp.doesCollide(this)){
				damage(pp.getPower());
				if(pp.getType().equals(ProjectileType.DEFAULT)){
					pp.explode();
				}
			}
		}
		for(Projectile pp:GameEngine.ennemyProjectiles){
			if(pp.doesCollide(this)){
				damage(pp.getPower());
				if(pp.getType().equals(ProjectileType.DEFAULT)){
					pp.explode();
				}
			}
		}
		anim.animate(0,false,false,0.5);
		if(trajectories!=null){
			followTrajectory();
		}
		super.update();
	}
	
	public void explode(){
		GameEngine.explosions.add(new Explosion(getX(),getY(),getWidth(),getHeight(),getCollider(),new Animation(new String[]{"explosion-0","explosion-1","explosion-2","explosion-3","explosion-4","explosion-5","explosion-6","explosion-7"}),0));
		destroy();
	}
	
	@Override
	public void destroy(){
		GameEngine.destroyedObstacles.add(this);
		super.destroy();
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public void setRotationSpeed(int rotationSpeed){
		this.rotationSpeed=rotationSpeed;
	}

	public int getRotationAngle(){
		return rotationAngle;
	}

	public void damage(int power){
		health-=power;
		if(health<=0){
			explode();
		}
	}
	
}
