package core.objects.projectiles;

import core.Collider;
import core.objects.GameObject;
import render.Animation;

public class Laser extends Projectile{
	
	private GameObject emitter;
	
	public Laser(GameObject emitter, int x, int width, Animation anim, int power, boolean ennemy) {
		super(x, emitter.getY(), width, emitter.getY(), null, anim, power, ennemy);
		collider = new Collider(x,y,width,height);
		this.emitter = emitter;
		type = ProjectileType.LASER;
	}
	
	@Override
	public void update(){
		if(y>0){
			y-=100;
			if(y<0){
				y=0;
			}
		}
		x = emitter.getX();
		height = emitter.getY()-y;
		collider.setX(x);
		collider.setY(y);
		collider.setWidth(width);
		collider.setHeight(height);
		anim.animate(0, true, false, 0.1);
	}
	
	@Override
	public boolean doesCollide(Collider other){
		if(super.doesCollide(other)){
			if(other.getY()+other.getHeight()>y){
				y = other.getY()+other.getHeight();
			}
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean doesCollide(GameObject other){
		return doesCollide(other.getCollider());
	}

}
