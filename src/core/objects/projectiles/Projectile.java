package core.objects.projectiles;

import core.Collider;
import core.GameEngine;
import core.objects.Explosion;
import core.objects.GameObject;
import render.Animation;

public class Projectile extends GameObject{

	protected ProjectileType type;
	
	protected boolean launched,ennemy,damageOnCollision;
	protected int power;

	public Projectile(int x, int y, int width, int height, Collider collider,Animation anim,int power,boolean ennemy){
		super(x,y,width,height,collider,anim);
		this.power=power;
		this.ennemy = ennemy;
		type = ProjectileType.DEFAULT;
		//uncomment for fun
		//GameEngine.textPlates.add(new TextPlate(this,0,height/2,0.3f,""+(char)(32+Math.random()*128)));
	}

	public void launch(){
		launched=true;
	}

	public void update(){
		if(launched){
			anim.animate(0,false,false,0.5);
			if(trajectories!=null){
				followTrajectory();
			}
			super.update();
		}
	}

	public void setPower(int power){
		this.power = power;
	}
	
	public ProjectileType getType(){
		return type;
	}

	public int getPower(){
		return power;
	}
	
	public void explode(){
		GameEngine.explosions.add(new Explosion(getX(),getY(),getWidth(),getHeight(),getCollider(),new Animation(new String[]{"explosion-0","explosion-1","explosion-2","explosion-3","explosion-4","explosion-5","explosion-6","explosion-7"}),0));
		destroy();
	}

	@Override
	public void destroy(){
		if(ennemy){
			destroyFromEnnemy();
		}else{
			destroyFromPlayer();
		}
		super.destroy();
	}

	public void destroyFromPlayer(){
		GameEngine.destroyedPlayerProjectiles.add(this);
	}

	public void destroyFromEnnemy(){
		GameEngine.destroyedEnnemyProjectiles.add(this);
	}

}
