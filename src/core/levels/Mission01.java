package core.levels;

import java.util.ArrayList;

import core.Collider;
import core.GameEngine;
import core.SoundDrivenGenerator;
import core.objects.Player;
import core.objects.TextPlate;
import core.objects.ennemies.Ennemy;
import core.objects.projectiles.Projectile;
import core.patterns.AsteroidPattern;
import core.patterns.DefaultPattern;
import core.patterns.LaserPattern;
import core.patterns.Pattern;
import render.Animation;
import render.layer.BackgroundLayer;
import render.layer.EnnemyLayer;
import render.layer.ObstacleLayer;
import render.layer.PlayerLayer;
import render.layer.ProjectileLayer;
import res.SpriteSheet;

public class Mission01 extends Level{

	public Mission01() {
		super();
	}

	@Override
	public void loadLevel() {
		// Sprites initialization
		SpriteSheet.loadSprite("player-0",0,0,64,64);
		SpriteSheet.loadSprite("player-1",0,64,64,64);
		SpriteSheet.loadSprite("player-2",0,128,64,64);
		SpriteSheet.loadSprite("reactor-0",0,192,64,32);
		SpriteSheet.loadSprite("reactor-1",0,224,64,32);
		SpriteSheet.loadSprite("reactor-2",0,256,64,32);
		SpriteSheet.loadSprite("particles-0",0,288,64,32);
		SpriteSheet.loadSprite("particles-1",0,320,64,32);
		SpriteSheet.loadSprite("particles-2",0,352,64,32);
		SpriteSheet.loadSprite("projectile-0",0,0,32,32);
		SpriteSheet.loadSprite("projectile-1",0,32,32,32);
		SpriteSheet.loadSprite("projectile-2",32,0,32,32);
		SpriteSheet.loadSprite("projectile-3",32,32,32,32);
		SpriteSheet.loadSprite("ennemy-0",0,0,64,64);
		SpriteSheet.loadSprite("ennemy-1",0,64,64,64);
		SpriteSheet.loadSprite("ennemy-projectile-0",64,0,32,32);
		SpriteSheet.loadSprite("ennemy-projectile-1",64,32,32,32);
		SpriteSheet.loadSprite("ennemy-projectile-2",96,0,32,32);
		SpriteSheet.loadSprite("ennemy-projectile-3",96,32,32,32);
		SpriteSheet.loadSprite("explosion-0",224,0,48,48);
		SpriteSheet.loadSprite("explosion-1",224,48,48,48);
		SpriteSheet.loadSprite("explosion-2",224,96,48,48);
		SpriteSheet.loadSprite("explosion-3",224,144,48,48);
		SpriteSheet.loadSprite("explosion-4",224+48,0,48,48);
		SpriteSheet.loadSprite("explosion-5",224+48,48,48,48);
		SpriteSheet.loadSprite("explosion-6",224+48,96,48,48);
		SpriteSheet.loadSprite("explosion-7",224+48,144,48,48);
		SpriteSheet.loadSprite("ennemy-module-0",64,64,32,32);
		SpriteSheet.loadSprite("ennemy-module-1",64,96,32,32);
		SpriteSheet.loadSprite("ennemy-module-2",96,64,32,32);
		SpriteSheet.loadSprite("ennemy-module-3",96,96,32,32);
		SpriteSheet.loadSprite("background-0", 0, 0, 594, 1024);
		SpriteSheet.loadSprite("background-anim-0", 594, 0, 594, 1024);
		SpriteSheet.loadSprite("background-overlay-0", 594, 0, 594, 1024);
		int roidsRow=8,roidsCol=roidsRow,
				roidWidth=80,roidHeight=88;
		for(int i=0;i<roidsRow*roidsCol;i++)SpriteSheet.loadSprite("asteroid-"+i,i%roidsCol*roidWidth,i/roidsRow*roidHeight,roidWidth,roidHeight);

		GameEngine.textPlates = new ArrayList<TextPlate>();
		int playerWidth=64,
				playerHeight=64,
				playerX=GameEngine.winW/2-playerWidth/2,
				playerY=GameEngine.winH-playerHeight,
				playerColliderWidth=2,
				playerColliderHeight=7;
		//le sprite fait le double du collider qui est placé au centre du sprite
		GameEngine.player = new Player(playerX,playerY,playerWidth,playerHeight,1000,20f,new Collider(playerX+playerWidth/2-playerColliderWidth/2,playerY+playerHeight/2+playerColliderHeight,playerColliderWidth,playerColliderHeight),new PlayerLayer("img/sprites/player-spritesheet.png"));
		GameEngine.player.setPattern(0, new DefaultPattern(GameEngine.player,10,2000,3,0,0,false));
		GameEngine.player.setPattern(1, new LaserPattern(GameEngine.player,1,false));
		//textPlates.add(new TextPlate(player,0,playerHeight/2,0.3f,"Player 1"));
		GameEngine.ennemies = new ArrayList<Ennemy>();
		GameEngine.destroyedEnnemies = new ArrayList<Ennemy>();
		GameEngine.ennemyProjectiles=new ArrayList<Projectile>();
		GameEngine.destroyedPlayerProjectiles = new ArrayList<Projectile>();
		GameEngine.destroyedEnnemyProjectiles = new ArrayList<Projectile>();
		GameEngine.generator = new SoundDrivenGenerator("level01",6000);
		Pattern asteroidGenerator=new AsteroidPattern(GameEngine.winW/2,0, 50,0, 10, 0, 0, true);
		Animation anim=new Animation(new String[]{"asteroid-0"});
		asteroidGenerator.setAnimation(anim);
		GameEngine.generator.addPattern(asteroidGenerator);
		/**
		Ennemy ennemy = new Ennemy(260,260,64,64,new Collider(5+260,5+260,54,54),200,Arrays.asList(new Linear(260,260,5,1,1)),new String[]{"ennemy-0","ennemy-1"});
		Module m = new Module(260+16,260+16,32,32,new Collider(260+16,260+16,32,32),new String[]{"ennemy-module-0","ennemy-module-1","ennemy-module-2","ennemy-module-3"},Arrays.asList(new Graviton(260+32-5,260+32-5,1,10,50),new Linear(260+32-5,260+32-5,5,1,1)),20,ennemy);
		m.addPattern(new Nova(m,10,20,3,0,true));
		ennemy.addModule(m);
		GameEngine.ennemies.add(ennemy);
		 **/
		layers.add(new BackgroundLayer(GameEngine.winW,GameEngine.winH,0,0,0,0,4, 
				new String[]{"background-0"}, 
				new String[]{"background-anim-0"},
				new String[]{"background-overlay-0"},
				"img/sprites/starry_bg.png"));		
		layers.add(GameEngine.player.layer);
		layers.add(new EnnemyLayer("img/sprites/ennemies-spritesheet.png"));
		layers.add(new ObstacleLayer("img/sprites/roids.png"));
		layers.add(new ProjectileLayer("img/sprites/projectiles-spritesheet.png"));
		GameEngine.sp.playSoundForever("level01");
	}

	@Override
	public void quit() {
		// TODO Auto-generated method stub

	}


}
