package core.levels;

import render.gui.GraphicalUserInterface;
import res.SpriteSheet;

public class MasterUI extends Level{

	private GraphicalUserInterface ui;
	
	public MasterUI(GraphicalUserInterface ui) {
		super();
		this.ui = ui;
	}

	@Override
	public void loadLevel() {

		// Characters Sprites initialization
		//" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~Â€Â�Â‚ÂƒÂ„Â…Â†Â‡ÂˆÂ‰ÂŠÂ‹ÂŒÂ�ÂŽÂ�Â�Â‘Â’Â“Â”Â•Â–Â—Â˜Â™ÂšÂ›ÂœÂ�ÂžÂŸ";
		int nbCharsInSheet=128,
				charSpriteSize=32,
				firstCharIdx=32,
				charPerRow=8;
		for(int i=firstCharIdx;i<nbCharsInSheet+firstCharIdx;i++){
		  SpriteSheet.loadSprite(""+(char)i,i%charPerRow*charSpriteSize,(i-firstCharIdx)/charPerRow*charSpriteSize,charSpriteSize,charSpriteSize);
		}

		ui.setLevel(this);
		ui.loadInterface();
		layers.add(ui);
	}

	@Override
	public void quit() {
		// TODO Auto-generated method stub
		
	}
	
}
