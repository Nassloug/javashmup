package core.levels;

import java.util.ArrayList;
import java.util.List;

import render.layer.Layer;

public abstract class Level {

	public List<Layer> layers;
	
	public Level(){
		layers = new ArrayList<Layer>();
	}
	
	public void drawLayers(){
		for(Layer layer : layers){
			layer.drawLayer();
		}
	}
	
	public void update(){
		for(Layer layer : layers){
			layer.update();
		}
	}
	
	public abstract void loadLevel();
	
	public abstract void quit();
	
}
