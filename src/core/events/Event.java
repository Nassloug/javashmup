package core.events;

public abstract class Event {

	protected boolean active;
	
	public Event(boolean active){
		this.active = active;
	}
	
	public abstract void happen();
}
