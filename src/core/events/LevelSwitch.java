package core.events;

import core.levels.Level;
import render.GameRenderer;

public class LevelSwitch extends Event {

	private Level actualLevel,nextLevel;
	
	public LevelSwitch(boolean active, Level actualLevel, Level nextLevel) {
		super(active);
		this.actualLevel = actualLevel;
		this.nextLevel = nextLevel;
	}

	@Override
	public void happen() {
		actualLevel.quit();
		GameRenderer.loadLevel(nextLevel);
	}

}
