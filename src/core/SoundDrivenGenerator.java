package core;

import core.patterns.Pattern;

public class SoundDrivenGenerator extends Generator{

	private String sound;
	private int amplitude;
	
	public SoundDrivenGenerator(String sound, int amplitude){
		super();
		this.sound = sound;
		this.amplitude = amplitude;
	}

	public void generate(){
		if(GameEngine.sp.getSound(sound).get(0).getAmplitude()>amplitude){
			for(Pattern p: generator){
				p.fire();
			}
		}
	}
	
	public void update(){
		generate();
	}
}
