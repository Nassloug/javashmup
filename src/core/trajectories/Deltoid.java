package core.trajectories;

import core.GameEngine;

public class Deltoid extends Trajectory{

	private double size;
	
	public Deltoid(int x, int y, double speed, double size) {
		super(x, y, 2*Math.PI*speed);
		this.size = size;
	}
	
	@Override
	public int nextX(){
		return (int)((2*Math.cos(getT())+Math.cos(2*getT()))*size*GameEngine.winW/10);
	}
	
	@Override
	public int nextY(){
		return (int)((2*Math.sin(getT())-Math.sin(2*getT()))*size*GameEngine.winH/10);
	}
	
	@Override
	public void update(){
		super.update();
	}
}