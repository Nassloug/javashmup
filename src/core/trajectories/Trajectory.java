package core.trajectories;

import render.GameRenderer;

public abstract class Trajectory {

	private int x,y;
	private double speed,t;
	
	public Trajectory(int x, int y, double speed){
		this.x = x;
		this.y = y;
		this.speed = speed;
		t=0;
	}
	
	public abstract int nextX();
	
	public abstract int nextY();
	
	public void setT(double t){
		this.t = t;
	}
	
	public double getT(){
		return t;
	}
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}
	
	public void setSpeed(double speed){
		this.speed = speed;
	}
	
	public double getSpeed(){
		return speed;
	}
	
	public void update(){
		t+=0.5/GameRenderer.REFRESH_RATE;
	}
		
}
