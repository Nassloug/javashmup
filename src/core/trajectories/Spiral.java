package core.trajectories;

import render.GameRenderer;

public class Spiral extends Trajectory {

	private double size;
	
	public Spiral(int x, int y, double speed, double size) {
		super(x, y, 2*Math.PI*speed);
		this.size = size;
	}
	
	@Override
	public int nextX(){
		return (int)(Math.sin(getT()*getSpeed())*size);
	}
	
	@Override
	public int nextY(){
		return (int)(Math.cos(getT()*getSpeed())*size);
	}
	
	@Override
	public void update(){
		size+=30.0/GameRenderer.REFRESH_RATE;
		super.update();
	}
}