package core.trajectories;

import render.GameRenderer;

public class Graviton extends Trajectory{

	private double size,maxSize;
	
	public Graviton(int x, int y, double speed, double size, double maxSize) {
		super(x, y, 2*Math.PI*speed);
		this.size = size;
		this.maxSize = maxSize;
	}
	
	@Override
	public int nextX(){
		return (int)(Math.sin(getT()*getSpeed())*size);
	}
	
	@Override
	public int nextY(){
		return (int)(Math.cos(getT()*getSpeed())*size);
	}
	
	@Override
	public void update(){
		if(size<maxSize){
			size+=30.0/GameRenderer.REFRESH_RATE;
		}
		super.update();
	}
}
