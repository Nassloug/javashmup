package core.trajectories;

public class Linear extends Trajectory{

	private float dx,dy;
	
	public Linear(int x, int y, double speed, float dx, float dy) {
		super(x, y, 2*Math.PI*speed);
		this.dx = dx;
		this.dy = dy;
	}
	
	@Override
	public int nextX(){
		return (int)(getT()*getSpeed()*dx);
	}
	
	@Override
	public int nextY(){
		return (int)(getT()*getSpeed()*dy);
	}
	
	@Override
	public void update(){
		super.update();
	}
	
}
