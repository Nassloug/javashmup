package core.trajectories;

import core.GameEngine;

public class Rosace extends Trajectory{

	private double size;
	private int k;
	
	public Rosace(int x, int y, double speed, double size, int k) {
		super(x, y, 2*Math.PI*speed);
		this.size = size;
		this.k = k;
	}
	
	@Override
	public int nextX(){
		return (int)(Math.cos(k*getT())*Math.sin(getT())*size*GameEngine.winH/10);
	}
	
	@Override
	public int nextY(){
		return (int)(Math.cos(k*getT())*Math.cos(getT())*size*GameEngine.winH/10);
	}
	
	@Override
	public void update(){
		super.update();
	}
	
}
