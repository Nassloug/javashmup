package core.trajectories;

import core.GameEngine;

public class Astroid extends Trajectory{

	private double size;
	
	public Astroid(int x, int y, double speed, float size) {
		super(x, y, 2*Math.PI*speed/size);
		this.size = size;
	}
	
	@Override
	public int nextX(){
		return (int)(Math.pow(Math.cos(getT()),3)*size*GameEngine.winW/10);
	}
	
	@Override
	public int nextY(){
		return (int)(Math.pow(Math.sin(getT()),3)*size*GameEngine.winH/10);
	}
	
	@Override
	public void update(){
		super.update();
	}
}
