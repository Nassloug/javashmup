package core.trajectories;

import core.GameEngine;

public class Eight extends Trajectory {
	
	private boolean vertical;
	private double size;
	
	public Eight(int x, int y, double speed, double size, boolean vertical) {
		super(x, y, 2*Math.PI*speed);
		this.vertical = vertical;
		this.size = size;
	}
	
	@Override
	public int nextX(){
		if(vertical){
			return (int)(Math.sin(getT())*size*GameEngine.winW/10);
		}else{
			return (int)(Math.sin(2*getT())*(size/2)*GameEngine.winW/10);
		}
	}
	
	@Override
	public int nextY(){
		if(vertical){
			return (int)(Math.sin(2*getT())*(size/2)*GameEngine.winH/10);
		}else{
			return (int)(Math.sin(getT())*size*GameEngine.winH/10);
		}
	}
	
	@Override
	public void update(){
		super.update();
	}
}