package core.patterns;

import core.Collider;
import core.GameEngine;
import core.objects.GameObject;
import core.objects.projectiles.Projectile;
import core.trajectories.Linear;

public class DefaultPattern extends Pattern{

	private int direction;

	public DefaultPattern(GameObject emitter, int power, double projectileSpeed, float fireRate, int rowRate, int offSet, boolean ennemy) {
		super(emitter,power,projectileSpeed,fireRate,rowRate,offSet,ennemy);
		if(ennemy){
			direction = -1;
		}else{
			direction = 1;
		}
	}

	@Override
	public void fire() {
		int projectileX=emitter.getX()+(int)(emitter.getFireHoleX()*emitter.getWidth())-projectileWidth/2,
				projectileY=emitter.getY()+(int)(emitter.getFireHoleY()*emitter.getHeight())-projectileHeight/2;
		Projectile p = new Projectile(projectileX,projectileY,projectileWidth,projectileHeight,new Collider(projectileX+projectileWidth/4,projectileY+projectileHeight/4,projectileWidth/2,projectileHeight/2),anim.clone(),power,ennemy);
		p.addTrajectory(new Linear(p.getX(),p.getY(),projectileSpeed,0, direction*(-1)));
		launchProjectile(p);
		p.launch();
		if(!ennemy)GameEngine.sp.playSound("projectile_launch_1");
	}
}
