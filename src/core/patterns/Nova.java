package core.patterns;

import core.Collider;
import core.objects.GameObject;
import core.objects.projectiles.Projectile;
import core.trajectories.Linear;

public class Nova extends Pattern{

	public Nova(GameObject emitter,int power,double projectileSpeed, float fireRate, int rowRate, int offSet, boolean ennemy) {
		super(emitter,power,projectileSpeed,fireRate,rowRate,offSet,ennemy);
	}

	@Override
	public void fire() {
		shotAngle=360;
		nbShots=36;
		for(int i=0;i<nbShots;i++){
			int projectileX=emitter.getX()+(int)(emitter.getFireHoleX()*emitter.getWidth())-projectileWidth/2,
				projectileY=emitter.getY()+(int)(emitter.getFireHoleY()*emitter.getHeight())-projectileHeight/2;
			Projectile p = new Projectile(projectileX,projectileY,projectileWidth,projectileHeight,new Collider(projectileX+projectileWidth/4,projectileY+projectileHeight/4,projectileWidth/2,projectileHeight/2),anim.clone(),power,ennemy);
			p.addTrajectory(new Linear(p.getX(),p.getY(),projectileSpeed,(float)(Math.cos(Math.toRadians((shotAngle/(nbShots*1f))*i))), (float)(Math.sin(Math.toRadians((shotAngle/(nbShots*1f))*i)))));
			launchProjectile(p);
			p.launch();
		}
	}
}
