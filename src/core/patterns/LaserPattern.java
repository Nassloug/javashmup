package core.patterns;

import core.objects.GameObject;
import core.objects.projectiles.Laser;
import render.Animation;

public class LaserPattern extends Pattern{

	private Laser laser;
	
	public LaserPattern(GameObject emitter, int power, boolean ennemy) {
		super(emitter, power, 0, 0, 0, 0, ennemy);
		anim = new Animation(new String[]{"projectile-0","projectile-1","projectile-2","projectile-3"}); 
	}

	@Override
	public void initFire(){
		if(laser==null){
			laser = new Laser(emitter,emitter.getX(),emitter.getWidth(),anim.clone(),power,ennemy);
			launchProjectile(laser);
		}
	}
	
	@Override 
	public void stopFire(){
		if(laser!=null){
			laser.destroy();
		}
		laser = null;
	}

	@Override
	public void fire() {}
}
