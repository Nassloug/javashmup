package core.patterns;

import core.Collider;
import core.GameEngine;
import core.objects.obstacles.Obstacle;
import core.trajectories.Linear;

public class AsteroidPattern extends Pattern{

	public AsteroidPattern(int x,int y, int power, double projectileSpeed, float fireRate, int rowRate, int offSet, boolean ennemy) {
		super(null,power,projectileSpeed,fireRate,rowRate,offSet, ennemy);
		this.x=x;
		this.y=y;
	}
	
	@Override
	public void fire() {
		anim.setSprite(new String[]{"asteroid-"+(int)(Math.random()*64)});
		double rand=1+Math.random()*Math.random()*1.25;
		int roidX=(int)(Math.random()*(GameEngine.winW+projectileWidth*rand)-projectileWidth*rand),
		    roidY=-(int)(projectileHeight*rand),
		    roidWidth=(int)(projectileWidth*rand),
		    roidHeight=(int)(projectileHeight*rand);
		Obstacle roid=new Obstacle(roidX,roidY,roidWidth,roidHeight,
		    new Collider(roidX,roidY,roidWidth,roidHeight),
		    anim.clone(),1,power,ennemy);
		roid.setRotationSpeed((int)(((Math.random()<0.5)?-1:1)*(1+Math.random()*1)));
		roid.setHealth((int) (roidWidth*roidHeight*0.7*0.001));
		roid.addTrajectory(new Linear(roid.getX(),roid.getY(),5,0, (float)(50/rand+Math.random())));
		GameEngine.obstacles.add(roid);
	}
}
