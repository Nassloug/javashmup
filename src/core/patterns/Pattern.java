package core.patterns;

import core.GameEngine;
import core.objects.GameObject;
import core.objects.projectiles.Projectile;
import render.Animation;
import render.GameRenderer;

public abstract class Pattern {

	protected GameObject emitter,receiver;
	protected boolean repeatOffSet,ennemy,stop,damageOnCollision;
	protected float fireRate;
	protected int x,y,nbShots,rowRate,power,offSet,timer,rowCounter,projectileWidth,projectileHeight;
	protected double shotAngle;
	protected double projectileSpeed;
	protected Animation anim;

	public Pattern(GameObject emitter, int power, double projectileSpeed, float fireRate, int rowRate, int offSet, boolean ennemy){
		this.emitter = emitter;
		this.power = power;
		this.projectileSpeed = GameEngine.winH*projectileSpeed/(100*GameRenderer.REFRESH_RATE);
		this.fireRate = fireRate;
		this.rowRate = rowRate;
		this.ennemy = ennemy;
		if(offSet<0){
			repeatOffSet = true;
			this.offSet = -offSet;
		}else{
			repeatOffSet = false;
			this.offSet = offSet;
		}
		stop = false;
		timer = -offSet;
		shotAngle = 0;
		x = y = nbShots = rowCounter = 0;
		anim = new Animation(new String[]{"projectile-0","projectile-1","projectile-2","projectile-3"});
		projectileWidth = anim.getAnimWidth();
		projectileHeight = anim.getAnimHeight();
	}

	public abstract void fire();

	public void initFire(){
		if(!stop){
			if(fireRate==0||(timer>=(GameEngine.winH/GameRenderer.REFRESH_RATE)/fireRate||timer==0)){
				fire();
				if(repeatOffSet&&rowRate!=0&&rowCounter>=rowRate-1){
					timer = -offSet;
					rowCounter = 0;
				}else{
					timer = 1;
					rowCounter ++;
				}
			}
			timer++;
		}
	}

	public void stopFire(){
		stop = true;
		timer = 0;
		rowCounter = 0;
	}

	public void resumeFire(){
		stop = false;
	}

	public void resetTimer(){
		timer = 0;
	}

	public void launchProjectile(Projectile p){
		if(ennemy){
			GameEngine.ennemyProjectiles.add(p);
		}else{
			GameEngine.player.projectiles.add(p);
		}
	}

	public void setAnimation(Animation anim){
		this.anim = anim;
		if(anim!=null){
			projectileWidth = anim.getAnimWidth();
			projectileHeight = anim.getAnimHeight();
		}
	}
}
