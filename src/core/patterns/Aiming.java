package core.patterns;

import core.Collider;
import core.objects.GameObject;
import core.objects.projectiles.Projectile;
import core.trajectories.Linear;

public class Aiming extends Pattern{
	
	public Aiming(GameObject emitter,GameObject receiver,int power, double projectileSpeed,float fireRate, int rowRate, int offSet, boolean ennemy) {
		super(emitter,power,projectileSpeed,fireRate,rowRate,offSet,ennemy);
		super.receiver = receiver;
	}

	@Override
	public void fire() {
		int projectileX=emitter.getX()+(int)(emitter.getFireHoleX()*emitter.getWidth())-projectileWidth/2,
			projectileY=emitter.getY()+(int)(emitter.getFireHoleY()*emitter.getHeight())-projectileHeight/2;
		Projectile p = new Projectile(projectileX,projectileY,projectileWidth,projectileHeight,new Collider(projectileX+projectileWidth/4,projectileY+projectileHeight/4,projectileWidth/2,projectileHeight/2),anim.clone(),power,ennemy);
		shotAngle=Math.toRadians(90)-Math.atan2(emitter.getX()-receiver.getX(),emitter.getY()-receiver.getY()-receiver.getHeight()/2);
		p.addTrajectory(new Linear(p.getX(),p.getY(),100, -(float)(Math.cos(shotAngle)), -(float)(Math.sin(shotAngle))));
		launchProjectile(p);
		p.launch();	
	}

}
