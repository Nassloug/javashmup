package res;

import java.util.HashMap;
import java.util.Map;

public class SpriteSheet {

	public static Map<String, Sprite> sprites = new HashMap<String, Sprite>();
	
	public static void loadSprite(String name, int x, int y, int width, int height){
		sprites.put(name, new Sprite(name, x, y, width, height));
	}
	
	public static Sprite getResource(String name){
		return sprites.get(name);
	}
	
	public static void freeResource(String name){
		sprites.remove(name);
	}
	
}
